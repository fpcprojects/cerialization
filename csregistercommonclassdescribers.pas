unit csRegisterCommonClassDescribers;

{$mode objfpc}{$H+}

interface

uses
  csModel,
  csCollectionDescriber,
  csStringListDescriber;

implementation

initialization
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TcsCollectionDescriber, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(TcsStringListDescriber, 100);
end.

