unit CerializationGenericTests;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  fpjson,
  testregistry,
  csModel,
  csJSONRttiStreamHelper,
  csGenericCollectionsDescriber,
  CerializationBasicTests;

type
  { TSerializeGenericListTests }

  TSerializeGenericListTests= class(TCustomSerializeTest)
  published
    procedure TestJSONToGenericObjectList;
    procedure TestJSONToGenericObjectListToJSON;

    procedure TestJSONToGenericIntegerList;
  end;

  TTestObjectList = specialize TObjectList<TTestObject>;
  TTestIntegerList = specialize TList<Integer>;

implementation

{ TSerializeGenericListTests }

procedure TSerializeGenericListTests.TestJSONToGenericObjectList;
var
  List: TTestObjectList;
begin
  List := TTestObjectList.Create(True);
  try
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"value1", "Prop2":"value1"},{"Prop1":"value2", "Prop2":"value2"}]', List);
    CheckEquals(2, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(not Assigned(List.Items[0]), 'First item is allocated while creating instances is not allowed');
    Check(not Assigned(List.Items[1]), 'Second item is allocated while creating instances is not allowed');

    // Tests with tcsdfCreateClassInstances
    List.Clear;
    // Test without tcsdfDynamicClassDescriptions flag
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"value1", "Prop2":"value1"},{"Prop1":"value2", "Prop2":"value2"}]', List, nil, [tcsdfCreateClassInstances]);
    CheckEquals(2, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(Assigned(List.Items[0]), 'First item in the list is not assigned?');
    Check(List.Items[0] is TTestObject, 'First item in the list is not a TTestObject');
    CheckEquals('value1', List[0].Prop1, 'prop1-value for first item');

    Check(Assigned(List.Items[1]), 'Second item in the list is not assigned?');
    Check(List.Items[1] is TTestObject, 'Second item in the list is not a TTestObject');
    CheckEquals('value2', List[1].Prop1, 'prop1-value for second item');

    List.Clear;
    // Test with tcsdfDynamicClassDescriptions flag
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"valuea", "Prop2":"valueb"},{"Prop1":"valueA", "Prop2":"valueB"}]', List, nil, [tcsdfCreateClassInstances, tcsdfDynamicClassDescriptions]);
    CheckEquals(2, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(Assigned(List.Items[0]), 'First item in the list is not assigned?');
    Check(List.Items[0] is TTestObject, 'First item in the list is not a TTestObject');
    CheckEquals('valuea', List[0].Prop1, 'prop1-value for first item');

    Check(Assigned(List.Items[1]), 'Second item in the list is not assigned?');
    Check(List.Items[1] is TTestObject, 'Second item in the list is not a TTestObject');
    CheckEquals('valueA', List[1].Prop1, 'prop1-value for second item');

    // Without tcsdfCreateClassInstances, but there are two instances already
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"Boo", "Prop2":"Ba"}]', List);
    CheckEquals(1, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(Assigned(List.Items[0]), 'First item in the list is not assigned?');
    Check(List.Items[0] is TTestObject, 'First item in the list is not a TTestObject');
    CheckEquals('Boo', List[0].Prop1, 'prop1-value for first item');
  finally
    List.Free;
  end;
end;

procedure TSerializeGenericListTests.TestJSONToGenericObjectListToJSON;
var
  List: TTestObjectList;
  JD: TJSONData;
  JSArray: TJSONArray;
begin
  List := TTestObjectList.Create(True);
  try
    List.Add(TTestObject.Create);
    List[0].Prop1 := '0ValueA';
    List[0].Prop2 := '0ValueB';

    List.Add(TTestObject.Create);
    List[1].Prop1 := '1ValueA';
    List[1].Prop2 := '1ValueB';

    JD := TJSONRttiStreamHelper.ObjectToJSON(List);
    try
      Check(JD.JSONType = jtArray, 'Streaming a TList<T> should result in an array');
      JSArray := JD as TJSONArray;
      CheckEquals(2, JSArray.Count);

      Check(JSArray.Items[0].JSONType = jtObject, 'The streamed list should contain objects');
      TestJSONProperty(JSArray.Items[0] as TJSONObject, 'Prop1', jtString, '0ValueA');
      TestJSONProperty(JSArray.Items[0] as TJSONObject, 'Prop2', jtString, '0ValueB');

      Check(JSArray.Items[1].JSONType = jtObject, 'The streamed list should contain objects');
      TestJSONProperty(JSArray.Items[1] as TJSONObject, 'Prop1', jtString, '1ValueA');
      TestJSONProperty(JSArray.Items[1] as TJSONObject, 'Prop2', jtString, '1ValueB');
    finally
      JD.Free;
    end;
  finally
    List.Free;
  end;
end;

procedure TSerializeGenericListTests.TestJSONToGenericIntegerList;
var
  List: TTestObjectList;
begin
  List := TTestObjectList.Create(True);
  try
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"value1", "Prop2":"value1"},{"Prop1":"value2", "Prop2":"value2"}]', List);
    CheckEquals(2, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(not Assigned(List.Items[0]), 'First item is allocated while creating instances is not allowed');
    Check(not Assigned(List.Items[1]), 'Second item is allocated while creating instances is not allowed');

    // Tests with tcsdfCreateClassInstances
    List.Clear;
    // Test without tcsdfDynamicClassDescriptions flag
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"value1", "Prop2":"value1"},{"Prop1":"value2", "Prop2":"value2"}]', List, nil, [tcsdfCreateClassInstances]);
    CheckEquals(2, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(Assigned(List.Items[0]), 'First item in the list is not assigned?');
    Check(List.Items[0] is TTestObject, 'First item in the list is not a TTestObject');
    CheckEquals('value1', List[0].Prop1, 'prop1-value for first item');

    Check(Assigned(List.Items[1]), 'Second item in the list is not assigned?');
    Check(List.Items[1] is TTestObject, 'Second item in the list is not a TTestObject');
    CheckEquals('value2', List[1].Prop1, 'prop1-value for second item');

    List.Clear;
    // Test with tcsdfDynamicClassDescriptions flag
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"valuea", "Prop2":"valueb"},{"Prop1":"valueA", "Prop2":"valueB"}]', List, nil, [tcsdfCreateClassInstances, tcsdfDynamicClassDescriptions]);
    CheckEquals(2, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(Assigned(List.Items[0]), 'First item in the list is not assigned?');
    Check(List.Items[0] is TTestObject, 'First item in the list is not a TTestObject');
    CheckEquals('valuea', List[0].Prop1, 'prop1-value for first item');

    Check(Assigned(List.Items[1]), 'Second item in the list is not assigned?');
    Check(List.Items[1] is TTestObject, 'Second item in the list is not a TTestObject');
    CheckEquals('valueA', List[1].Prop1, 'prop1-value for second item');

    // Without tcsdfCreateClassInstances, but there are two instances already
    TJSONRttiStreamHelper.JSONStringToObject('[{"Prop1":"Boo", "Prop2":"Ba"}]', List);
    CheckEquals(1, List.Count, 'JSONToObject return an invalid amount of items in the list');
    Check(Assigned(List.Items[0]), 'First item in the list is not assigned?');
    Check(List.Items[0] is TTestObject, 'First item in the list is not a TTestObject');
    CheckEquals('Boo', List[0].Prop1, 'prop1-value for first item');
  finally
    List.Free;
  end;
end;

initialization
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(specialize TcsGenericCollectionTListObjectDescriber<TTestObject>, 100);
  TcsClassDescriberFactory.Instance.RegisterClassDescriber(specialize TcsGenericCollectionTListDescriber<Integer>, 100);

  RegisterTest(TSerializeGenericListTests);
end.

