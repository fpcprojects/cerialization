unit CerializationStringlistTest;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  testregistry,
  fpjson,
  csModel,
  csJSONRttiStreamHelper,
  CerializationBasicTests;

type

  { TSerializeStringListTests }

  TSerializeStringListTests= class(TCustomSerializeTest)
  protected
    procedure ObjectToJsonShouldExcept;
  published
    procedure TestStringListToJSON;
    procedure TestJSONToStringList;

    procedure TestObjectJSONToStringListFailure;
  end;

implementation

{ TSerializeStringListTests }

procedure TSerializeStringListTests.ObjectToJsonShouldExcept;
var
  List: TStrings;
begin
  List := TStringList.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{}', List);
  finally
    List.Free;
  end;
end;

procedure TSerializeStringListTests.TestStringListToJSON;
var
  List: TStringList;
  JSArray: TJSONArray;
  i: Integer;
begin
  List := TStringList.Create;
  try
    List.Add('Item1');
    List.Add('Item2');
    List.Add('Item3');
    List.Values['item4'] := 'value4';

    JSArray := TJSONRttiStreamHelper.ObjectToJSon(List) as TJSONArray;
    try
      Check(Assigned(JSArray), 'ObjectToJSon returned nil');
      CheckEquals(4, JSArray.Count, 'Invalid amount of items in js-array');
      for i := 0 to List.Count -1 do
        begin
        Check(JSArray.Items[i].JSONType = jtString, 'Array-element is not a string');
        Check(JSArray.Items[i].AsString = List.Strings[i], 'Array-element contains not the proper string');
        end;
    finally
      JSArray.Free;
    end;
  finally
    List.Free;
  end;
end;

procedure TSerializeStringListTests.TestJSONToStringList;
var
  List: TStrings;
begin
  List := TStringList.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('["one", "two=three", "four"]', List);

    CheckEquals(3, List.Count, 'Invalid amount of items in stringlist');
    CheckEquals('one', List.Strings[0], 'JSONToObject did not translate the string ''one'' properly');
    CheckEquals('three', List.Values['two'], 'JSONToObject did not translate the key-value ''three'' properly');
    CheckEquals('four', List.Strings[2], 'JSONToObject did not translate the string ''four'' properly');
  finally
    List.Free;
  end;
end;

procedure TSerializeStringListTests.TestObjectJSONToStringListFailure;
begin
  CheckException(@ObjectToJsonShouldExcept, EcsStreamException, 'Json-array expected but got ''{}''');
end;

initialization
  RegisterTest(TSerializeStringListTests);
end.
