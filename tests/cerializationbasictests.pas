unit CerializationBasicTests;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  db,
  Math,
  fpcunit,
  typinfo,
  testregistry,
  csCollectionDescriber,
  csRegisterCommonClassDescribers,
  fpjson,
  jsonparser,
  csJSONRttiStreamHelper,
  csModel;

type

  { TCustomSerializeTest }

  TCustomSerializeTest = class(TtestCase)
  protected
    function TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype): TJSONData; overload;
    procedure TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: string); overload;
    procedure TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: Integer); overload;
    procedure TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: Extended); overload;
    procedure TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: Boolean); overload;
  end;

  { TSerializeSimpleObjectTests }

  TSerializeSimpleObjectTests= class(TCustomSerializeTest)
  published
    procedure TestSimpleClassToJSON;
    procedure TestJSONToSimpleClass;
    procedure TestJSONStringToSimpleClass;
    procedure TestEnumeration;
    procedure TestFloat;
    procedure TestBoolean;
    procedure TestDatetime;
  end;

  { TSerializeArraysInObjectTests }

  TSerializeArraysInObjectTests= class(TTestCase)
  published
    procedure TestSimpleArrayToJSON;
    procedure TestSimpleStringArrayToJSON;

    procedure TestJSONStringToSimpleArray;
    procedure TestJSONStringToSimpleStringArray;
  end;

  { TSerializeSubObjectsTests }

  TSerializeSubObjectsTests= class(TCustomSerializeTest)
  published
    procedure TestSimpleSubObject;
    procedure TestJSONToSimpleSubObject;
    procedure TestJSONToSimpleSubObjectNotAllowed;
  end;

  { TDescriptionTests }

  TDescriptionTests = class(TCustomSerializeTest)
  private
    function GetEnumValueAsString(AnInstance: TObject; ADescription: TcsStreamDescription; out AValue: string): boolean;
    function SetEnumValueAsString(AnInstance: TObject; ADescription: TcsStreamDescription; const AValue: string): boolean;
  published
    procedure TestExportNameStyle;
    procedure TestParentExportNameStyle;
    // There was a bug in which the parent of sub-classes was not set, so that
    // the ImportNameStyle was not inherited.
    procedure TestParentOfSubClasses;
    procedure TestClone;
    procedure TestDefaultExportNameStyle;
    procedure TestImportNameStyle;

    procedure TestEnumerationOnSetAsString;
    procedure TestEnumerationOnGetAsString;
  end;

  { TSerializeCollectionTests }

  TSerializeCollectionTests= class(TCustomSerializeTest)
  published
    procedure TestNamedCollectionToJSON;
    procedure TestNamedCollectionToJSONWithAdaptedListDescription;
    procedure TestNamedCollectionToJSONWithClonedDescription;
    procedure TestNamedCollectionToJSONArray;
    procedure TestCollectionAsPropertyToJSON;
    procedure TestCollectionAsPropertyToJSONArray;

    procedure TestJSONToNamedCollection;
    procedure TestJSONToNamedCollectionWithAdaptedListDescription;
    procedure TestJSONArrayToNamedCollection;
    procedure TestJSONToCollectionAsProperty;
    procedure TestJSONArrayToCollectionAsProperty;
  end;

  { TStreamHelperFlagsTests }

  TStreamHelperFlagsTests= class(TCustomSerializeTest)
  published
    procedure TestCollectionAsListFlag;
    // Same as the test above, but using TJSONRttiStreamHelper
    procedure TestCollectionAsListFlagClassFunction;
    procedure TestDisableRetrieveDescriberFlag;
  end;

  { TSerializePropertiesTests }

  TSerializePropertiesTests= class(TCustomSerializeTest)
  published
    procedure TestClassToJSONReadonlyProperty;
    procedure TestJSONToClassReadonlyProperty;
  end;

  { TSerializeFilterTests }

  TSerializeFilterTests= class(TCustomSerializeTest)
  private
    function DoFilterProp(AnInstance: TObject; ADescription: TcsStreamDescription): boolean;
    function DoListFilterProp(AnInstance: TObject; ADescription: TcsStreamDescription): boolean;
  published
    procedure TestPropertyFilter;
    procedure TestListFilter;
  end;


  { TClassDescriptorFactoryTests }

  TClassDescriptorFactoryTests= class(TCustomSerializeTest)
  published
    procedure TestGlobalAndLocalFactories;
  end;

  { TTestObject }

  TTestObject = class
  private
    FInt1: integer;
    FProp1: string;
    FProp2: string;
    FCamelCaseProp: integer;
  published
    property Prop1: string read FProp1 write FProp1;
    property Prop2: string read FProp2 write FProp2;
    property Int1: integer read FInt1 write FInt1;
    property CamelCaseProp: integer read FCamelCaseProp write FCamelCaseProp;
  end;

implementation

type

  { TTestArrayObject }

  TIntArray = array of integer;
  TTestArrayObject = class
  private
    FProp1: string;
    FArrOfInt: TIntArray;
  published
    property Prop1: string read FProp1 write FProp1;
    property ArrOfInt: TIntArray read FArrOfInt write FArrOfInt;
  end;

  { TTestStringArrayObject }

  TStrArray = array of String;
  TTestStringArrayObject = class
  private
    FProp1: string;
    FArrOfStr: TStrArray;
  published
    property Prop1: string read FProp1 write FProp1;
    property ArrOfStr: TStrArray read FArrOfStr write FArrOfStr;
  end;

  { TTestSubObjectObject }

  TTestSubObjectObject = class
  private
    FProp1: TTestObject;
    FProp2: TTestObject;
    FProp3: TTestObject;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Obj1: TTestObject read FProp1 write FProp1;
    property Obj2: TTestObject read FProp2 write FProp2;
    property Obj3: TTestObject read FProp3;
  end;

  { TTestCollectionProperty }

  TTestCollectionProperty = class
  private
    FColl: TCollection;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Coll: TCollection read FColl;
  end;

  { TTestBooleanProperty }

  TTestBooleanProperty = class
  private
    FBool1: Boolean;
    FBool2: Boolean;
  published
    property Bool1: Boolean read FBool1 write FBool1;
    property Bool2: Boolean read FBool2 write FBool2;
  end;

{ TSerializePropertiesTests }

type

  { TReadWriteOnlyTestObject }

  TReadWriteOnlyTestObject = class
  private
    FReadOnlyProp: string;
    FWriteOnlyProp: string;
  published
    property ReadOnlyProp: string read FReadOnlyProp;
    property WriteOnlyProp: string write FWriteOnlyProp;
  end;

type

  { TTestEnumObject }

  TTestEnum = (Enum1, Enum2, Enum3, Enum4);
  TTestEnumObject= class
  private
    FEnumProp1: TTestEnum;
    FEnumProp2: TTestEnum;
  published
    property EnumProp1: TTestEnum read FEnumProp1 write FEnumProp1;
    property EnumProp2: TTestEnum read FEnumProp2 write FEnumProp2;
  end;

  { TTestDatetimeObject }

  TTestDatetimeObject= class
  private
    FDatetimeProp1: TDateTime;
    FDatetimeProp2: TDateTime;
    FDateProp1: TDate;
    FDateProp2: TDate;
  published
    property DatetimeProp1: TDateTime read FDatetimeProp1 write FDatetimeProp1;
    property DatetimeProp2: TDateTime read FDatetimeProp2 write FDatetimeProp2;
    property DateProp1: TDate read FDateProp1 write FDateProp1;
    property DateProp2: TDate read FDateProp2 write FDateProp2;
  end;

  TTestFloatObject= class
  private
    FFloatProp1: Double;
    FFloatProp2: Double;
  published
    property FloatProp1: Double read FFloatProp1 write FFloatProp1;
    property FloatProp2: Double read FFloatProp2 write FFloatProp2;
  end;

{ TClassDescriptorFactoryTests }

procedure TClassDescriptorFactoryTests.TestGlobalAndLocalFactories;
var
  JD: TJSONData;
  Serializer: TJSONRttiStreamClass;
  Coll: TCollection;
  Item: TNamedItem;
begin
  // Make sure that the ClassDescriber for collections in the global factory creates lists
  (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];

  Coll := TCollection.Create(TNamedItem);
  try
    Item := TNamedItem.Create(Coll);
    Item.Name := 'One of the items';
    Item := TNamedItem.Create(Coll);
    Item.Name := 'Another one of the items';

    // Use the global factory
    Serializer := TJSONRttiStreamClass.Create;
    try
      JD := Serializer.ObjectToJSon(Coll);
      try
        Check(Assigned(JD), 'ObjectToJSon returned nil');
        Check(JD.JSONType = jtArray, 'The collection has been converted to something else then an array.');
      finally
        JD.Free;
      end;
    finally
      Serializer.Free;
    end;

    // Use an empty factory
    Serializer := TJSONRttiStreamClass.Create;
    try
      Serializer.Describer.SetClassDescriberFactory(TcsClassDescriberFactory.Create, True);
      JD := Serializer.ObjectToJSon(Coll);
      try
        Check(Assigned(JD), 'ObjectToJSon returned nil');
        Check(JD.JSONType = jtObject, 'The collection has been converted to something else then an array.');
        CheckEquals(0, TJSONObject(JD).Count, 'The class is not empty, while it should without a class-describer');
      finally
        JD.Free;
      end;
    finally
      Serializer.Free;
    end;

    // Clone the global factory.
    Serializer := TJSONRttiStreamClass.Create;
    try
      Serializer.Describer.SetClassDescriberFactory(TcsClassDescriberFactory.Create, True);
      Serializer.Describer.ClassDescriberFactory.Assign(TcsClassDescriberFactory.Instance);

      // The global ClassDescriber does not return lists anymore. (The clone we just made
      // should, though)
      (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [];

      JD := Serializer.ObjectToJSon(Coll);
      try
        Check(Assigned(JD), 'ObjectToJSon returned nil');
        Check(JD.JSONType = jtArray, 'The collection has been converted to something else then an array.');
      finally
        JD.Free;
      end;
    finally
      Serializer.Free;
    end;
  finally
    Coll.Free;
  end;
end;

{ TStreamHelperFlagsTests }

procedure TStreamHelperFlagsTests.TestCollectionAsListFlagClassFunction;
var
  Coll: TCollection;
  Item: TNamedItem;
  JS: TJSONData;
  JSArray: TJSONArray;
begin
  Coll := TCollection.Create(TNamedItem);
  try
    Item := TNamedItem.Create(Coll);
    Item.Name := 'One of the items';
    Item := TNamedItem.Create(Coll);
    Item.Name := 'Another one of the items';

    (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];

    JS := TJSONRttiStreamHelper.ObjectToJSon(Coll, nil, [tcsdfDynamicClassDescriptions]);
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      Check(JS.JSONType=jtArray, 'ObjectToJSon did not return an array');
      JSArray := JS as TJSONArray;
      CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');

      Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
      Check(JSArray.Items[1].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');

      TestJSONProperty(TJSONObject(JSArray.Items[0]), 'Name', jtString, 'One of the items');
      TestJSONProperty(TJSONObject(JSArray.Items[1]), 'Name', jtString, 'Another one of the items');
    finally
      JS.Free;
    end;
  finally
    Coll.Free;
  end;
end;

procedure TStreamHelperFlagsTests.TestDisableRetrieveDescriberFlag;
var
  Coll: TCollection;
  Item: TNamedItem;
  JS: TJSONData;
  JSArray: TJSONArray;
begin
  Coll := TCollection.Create(TNamedItem);
  try
    Item := TNamedItem.Create(Coll);
    Item.Name := 'One of the items';
    Item := TNamedItem.Create(Coll);
    Item.Name := 'Another one of the items';

    (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];

    JS := TJSONRttiStreamHelper.ObjectToJSon(Coll, nil);
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      Check(JS.JSONType=jtArray, 'ObjectToJSon did not return an array');
      JSArray := JS as TJSONArray;
      CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');
      Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
      CheckEquals(0, TJSONObject(JSArray.Items[0]).Count, 'The object in the array should have no properties');
      Check(JSArray.Items[0].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');
      CheckEquals(0, TJSONObject(JSArray.Items[1]).Count, 'The object in the array should have no properties');
    finally
      JS.Free;
    end;
  finally
    Coll.Free;
  end;
end;

procedure TStreamHelperFlagsTests.TestCollectionAsListFlag;
var
  Coll: TCollection;
  Item: TNamedItem;
  JS: TJSONData;
  JSArray: TJSONArray;
  Serializer: TJSONRttiStreamClass;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Coll := TCollection.Create(TNamedItem);
    try
      Item := TNamedItem.Create(Coll);
      Item.Name := 'One of the items';
      Item := TNamedItem.Create(Coll);
      Item.Name := 'Another one of the items';

      (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];

      Serializer.Describer.Flags := [tcsdfDynamicClassDescriptions];
      JS := Serializer.ObjectToJSon(Coll);
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        Check(JS.JSONType=jtArray, 'ObjectToJSon did not return an array');
        JSArray := JS as TJSONArray;
        CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');

        Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
        Check(JSArray.Items[1].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');

        TestJSONProperty(TJSONObject(JSArray.Items[0]), 'Name', jtString, 'One of the items');
        TestJSONProperty(TJSONObject(JSArray.Items[1]), 'Name', jtString, 'Another one of the items');
      finally
        JS.Free;
      end;
    finally
      Coll.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

constructor TTestSubObjectObject.Create;
begin
  FProp3 := TTestObject.Create;
end;

destructor TTestSubObjectObject.Destroy;
begin
  FProp3.Free;
  inherited Destroy;
end;

procedure TSerializePropertiesTests.TestClassToJSONReadonlyProperty;
var
  Inst: TReadWriteOnlyTestObject;
  JS: TJSONObject;
begin
  Inst := TReadWriteOnlyTestObject.Create;
  try
    Inst.WriteOnlyProp:='WriteOnly';
    Inst.FReadOnlyProp:='ReadOnly';

    JS := TJSONRttiStreamHelper.ObjectToJSon(Inst) as TJSONObject;
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      TestJSONProperty(JS, 'ReadOnlyProp', jtString, 'ReadOnly');
      Check(not Assigned(JS.Find('WriteOnlyProp')), 'WriteOnlyProp should have been omitted from the resulting JSON');
    finally
      JS.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializePropertiesTests.TestJSONToClassReadonlyProperty;
var
  Inst: TReadWriteOnlyTestObject;
  JS: TJSONData;
begin
  JS := GetJSON('{"WriteOnlyProp":"WriteOnly","ReadOnlyProp":"ReadOnly"}');
  try
    Inst := TReadWriteOnlyTestObject.Create;
    try
      TJSONRttiStreamHelper.JSONToObject(JS as TJSONObject, Inst);

      CheckEquals('WriteOnly', Inst.FWriteOnlyProp, 'JSONToObject did not get the property ''prop1''');
      CheckEquals('', Inst.ReadOnlyProp, 'JSONToObject did not get the property ''prop2''');
    finally
      Inst.Free;
    end;
  finally
    Js.Free;
  end;
end;

{ TTestCollectionProperty }

constructor TTestCollectionProperty.Create;
begin
  FColl := TCollection.Create(TNamedItem);
end;

destructor TTestCollectionProperty.Destroy;
begin
  FColl.Free;
  inherited Destroy;
end;

{ TSerializeSubObjectsTests }

procedure TSerializeSubObjectsTests.TestSimpleSubObject;
var
  Inst: TTestSubObjectObject;
  JS: TJSONObject;
  JSValue: TJSONData;
begin
  Inst := TTestSubObjectObject.Create;
  try
    Inst.Obj1 := TTestObject.Create;
    try
      Inst.Obj1.Prop1:='Propje1';

      JS := TJSONRttiStreamHelper.ObjectToJSon(Inst) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        JSValue := TestJSONProperty(JS, 'Obj1', jtObject);
        TestJSONProperty(JSValue as TJSONObject, 'Prop1', jtString, 'Propje1');
        JSValue := TestJSONProperty(JS, 'Obj2', jtNull);
      finally
        JS.Free;
      end;
    finally
      Inst.Obj1.Free;
    end;

  finally
    Inst.Free;
  end;
end;

procedure TSerializeSubObjectsTests.TestJSONToSimpleSubObject;
var
  Inst: TTestSubObjectObject;
begin
  Inst := TTestSubObjectObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"Obj1":{"Prop1":"SubProp"},"Obj2":null,"Obj3":{"Prop2":"Read only SubProp"}}', Inst, nil, [tcsdfCreateClassInstances]);

    CheckNotNull(Inst.Obj1, 'JSONToObject failed to create sub-object instance');
    CheckEquals('SubProp', Inst.Obj1.Prop1, 'JSONToObject did not get the property ''obj1.prop1''');
    CheckNull(Inst.Obj2, 'JSONToObject created an object-instance it should not');
    CheckEquals('Read only SubProp', Inst.Obj3.Prop2, 'JSONToObject did not get the property ''obj3.prop2''');
  finally
    Inst.Obj1.Free;
    Inst.Free;
  end;
end;

procedure TSerializeSubObjectsTests.TestJSONToSimpleSubObjectNotAllowed;
var
  Inst: TTestSubObjectObject;
begin
  Inst := TTestSubObjectObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"Obj1":{"Prop1":"SubProp"},"Obj2":null,"Obj3":{"Prop2":"Read only SubProp"}}', Inst);

    CheckNull(Inst.Obj1, 'JSONToObject created an object-instance it is not allowed to');
    CheckNull(Inst.Obj2, 'JSONToObject created an object-instance it should not');
    CheckEquals('Read only SubProp', Inst.Obj3.Prop2, 'JSONToObject did not get the property ''obj3.prop2''');
  finally
    Inst.Obj1.Free;
    Inst.Free;
  end;

end;

procedure TSerializeCollectionTests.TestNamedCollectionToJSON;
var
  Coll: TCollection;
  Item: TNamedItem;
  JS: TJSONObject;
  JSArray: TJSONArray;
begin
  Coll := TCollection.Create(TNamedItem);
  try
    Item := TNamedItem.Create(Coll);
    Item.Name := 'One of the items';
    Item := TNamedItem.Create(Coll);
    Item.Name := 'Another one of the items';

    (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [];

    JS := TJSONRttiStreamHelper.ObjectToJSon(Coll) as TJSONObject;
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      JSArray := TestJSONProperty(JS, 'Items', jtArray) as TJSONArray;
      CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');
      Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
      CheckEquals(0, TJSONObject(JSArray.Items[0]).Count, 'The object in the array should have no properties');
      Check(JSArray.Items[0].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');
      CheckEquals(0, TJSONObject(JSArray.Items[1]).Count, 'The object in the array should have no properties');
    finally
      JS.Free;
    end;
  finally
    Coll.Free;
  end;
end;

procedure TSerializeCollectionTests.TestNamedCollectionToJSONWithAdaptedListDescription;
var
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
  Coll: TCollection;
  Item: TNamedItem;
  JSArray: TJSONArray;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Coll := TCollection.Create(TNamedItem);
    try
      Item := TNamedItem.Create(Coll);
      Item.Name := 'One of the items';
      Item := TNamedItem.Create(Coll);
      Item.Name := 'Another one of the items';

      Description := Serializer.DescriptionStore.GetDescription(TCollection);
      Description.Properties.FindByPropertyName('Items').ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

      JS := Serializer.ObjectToJSon(Coll) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        JSArray := TestJSONProperty(JS, 'Items', jtArray) as TJSONArray;
        CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');

        Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
        Check(JSArray.Items[1].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');

        TestJSONProperty(TJSONObject(JSArray.Items[0]), 'Name', jtString, 'One of the items');
        TestJSONProperty(TJSONObject(JSArray.Items[1]), 'Name', jtString, 'Another one of the items');
      finally
        JS.Free;
      end;
    finally
      Coll.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TSerializeCollectionTests.TestNamedCollectionToJSONWithClonedDescription;
var
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
  Coll: TCollection;
  Item: TNamedItem;
  JSArray: TJSONArray;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Coll := TCollection.Create(TNamedItem);
    try
      Item := TNamedItem.Create(Coll);
      Item.Name := 'One of the items';
      Item := TNamedItem.Create(Coll);
      Item.Name := 'Another one of the items';

      Description := Serializer.DescriptionStore.GetDescription(TCollection);
      Description.Properties.FindByPropertyName('Items').ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

      JS := Serializer.ObjectToJSon(Coll) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        JSArray := TestJSONProperty(JS, 'Items', jtArray) as TJSONArray;
        CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');

        Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
        Check(JSArray.Items[1].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');

        TestJSONProperty(TJSONObject(JSArray.Items[0]), 'Name', jtString, 'One of the items');
        TestJSONProperty(TJSONObject(JSArray.Items[1]), 'Name', jtString, 'Another one of the items');
      finally
        JS.Free;
      end;
    finally
      Coll.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TSerializeCollectionTests.TestNamedCollectionToJSONArray;
var
  JS: TJSONData;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
  Coll: TCollection;
  Item: TNamedItem;
  JSArray: TJSONArray;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];
    Coll := TCollection.Create(TNamedItem);
    try
      Item := TNamedItem.Create(Coll);
      Item.Name := 'One of the items';
      Item := TNamedItem.Create(Coll);
      Item.Name := 'Another one of the items';

      Description := Serializer.DescriptionStore.GetDescription(TCollection);
      Description.ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

      JS := Serializer.ObjectToJSon(Coll);
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        Check(jtArray = JS.JSONType, 'TCollection has not been converted into an array');
        JSArray := JS as TJSONArray;
        CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');

        Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
        Check(JSArray.Items[1].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');

        TestJSONProperty(TJSONObject(JSArray.Items[0]), 'Name', jtString, 'One of the items');
        TestJSONProperty(TJSONObject(JSArray.Items[1]), 'Name', jtString, 'Another one of the items');
      finally
        JS.Free;
      end;
    finally
      Coll.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TSerializeCollectionTests.TestCollectionAsPropertyToJSONArray;
var
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  JSArray: TJSONArray;
  Inst: TTestCollectionProperty;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];

    Description := Serializer.DescriptionStore.GetDescription(TTestCollectionProperty);
    Description.Properties.FindByPropertyName('Coll').ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

    Inst := TTestCollectionProperty.Create;
    try
      (Inst.Coll.Add as TNamedItem).Name := 'Item1';
      (Inst.Coll.Add as TNamedItem).Name := 'Item2';
      (Inst.Coll.Add as TNamedItem).Name := 'Item3';

      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try

        Check(Assigned(JS), 'ObjectToJSon returned nil');
        JSArray := TestJSONProperty(JS, 'Coll', jtArray) as TJSONArray;
        CheckEquals(3, JSArray.Count, 'Invalid amount of items in js-array');

        Check(JSArray.Items[0].JSONType = jtObject, 'Array-element is not an object');
        CheckEquals(1, TJSONObject(JSArray.Items[0]).Count, 'Object has properties while it should not');
        TestJSONProperty(TJSONObject(JSArray.Items[0]), 'Name', jtString, 'Item1');

        Check(JSArray.Items[1].JSONType = jtObject, 'Array-element is not an object');
        CheckEquals(1, TJSONObject(JSArray.Items[1]).Count, 'Object has properties while it should not');
        TestJSONProperty(TJSONObject(JSArray.Items[1]), 'Name', jtString, 'Item2' );

        Check(JSArray.Items[2].JSONType = jtObject, 'Array-element is not an object');
        CheckEquals(1, TJSONObject(JSArray.Items[2]).Count, 'Object has properties while it should not');
        TestJSONProperty(TJSONObject(JSArray.Items[2]), 'Name', jtString, 'Item3');
      finally
        JS.Free;
      end;
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TSerializeCollectionTests.TestCollectionAsPropertyToJSON;
var
  Inst: TTestCollectionProperty;
  JS: TJSONObject;
  JSObject: TJSONObject;
  JSArray: TJSONArray;
begin
  (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [];
  Inst := TTestCollectionProperty.Create;
  try
    (Inst.Coll.Add as TNamedItem).Name := 'Item1';
    (Inst.Coll.Add as TNamedItem).Name := 'Item2';
    (Inst.Coll.Add as TNamedItem).Name := 'Item3';

    JS := TJSONRttiStreamHelper.ObjectToJSon(Inst) as TJSONObject;
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      JSObject := TestJSONProperty(JS, 'Coll', jtObject) as TJSONObject;
      JSArray := TestJSONProperty(JSObject, 'Items', jtArray) as TJSONArray;
      CheckEquals(3, JSArray.Count, 'Invalid amount of items in js-array');
      Check(JSArray.Items[0].JSONType = jtObject, 'Array-element is not an object');
      CheckEquals(0, TJSONObject(JSArray.Items[0]).Count, 'Object has properties while it should not');
      Check(JSArray.Items[1].JSONType = jtObject, 'Array-element is not an object');
      CheckEquals(0, TJSONObject(JSArray.Items[1]).Count, 'Object has properties while it should not');
      Check(JSArray.Items[2].JSONType = jtObject, 'Array-element is not an object');
      CheckEquals(0, TJSONObject(JSArray.Items[2]).Count, 'Object has properties while it should not');
    finally
      JS.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializeCollectionTests.TestJSONToNamedCollection;
var
  Coll: TCollection;
begin
  (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [];
  Coll := TCollection.Create(TNamedItem);
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"Items" : [{"Name": "JoJo"}, {"Name":"Second name"}]}', Coll);

    CheckEquals(2, Coll.Count, 'JSONToObject return an invalid amount of items in the collection');
    Check(Assigned(Coll.Items[0]), 'First item in the collection is not assigned?');
    Check(Coll.Items[0] is TNamedItem, 'First item in the collection is not a TNamedItem');
    CheckEquals('', TNamedItem(Coll.Items[0]).Name, 'Name of first item should be nil, because there is no description given for the elements');

    Check(Assigned(Coll.Items[1]), 'Second item in the collection is not assigned?');
    Check(Coll.Items[1] is TNamedItem, 'Second item in the collection is not a TNamedItem');
    CheckEquals('', TNamedItem(Coll.Items[1]).Name, 'Name of second item should be nil, because there is no description given for the elements');
  finally
    Coll.Free;
  end;
end;

procedure TSerializeCollectionTests.TestJSONToNamedCollectionWithAdaptedListDescription;
var
  Coll: TCollection;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Coll := TCollection.Create(TNamedItem);
    try
      Description := Serializer.DescriptionStore.GetDescription(TCollection);
      Description.Properties.FindByPropertyName('Items').ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

      Serializer.JSONStringToObject('{"Items" : [{"Name": "JoJo"}, {"Name":"Second name"}]}', Coll);

      CheckEquals(2, Coll.Count, 'JSONToObject return an invalid amount of items in the collection');
      Check(Assigned(Coll.Items[0]), 'First item in the collection is not assigned?');
      Check(Coll.Items[0] is TNamedItem, 'First item in the collection is not a TNamedItem');
      CheckEquals('JoJo', TNamedItem(Coll.Items[0]).Name, 'Name of first item should be nil, because there is no description given for the elements');

      Check(Assigned(Coll.Items[1]), 'Second item in the collection is not assigned?');
      Check(Coll.Items[1] is TNamedItem, 'Second item in the collection is not a TNamedItem');
      CheckEquals('Second name', TNamedItem(Coll.Items[1]).Name, 'Name of second item should be nil, because there is no description given for the elements');
    finally
      Coll.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TSerializeCollectionTests.TestJSONArrayToNamedCollection;
var
  Coll: TCollection;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];
    Coll := TCollection.Create(TNamedItem);
    try
      Description := Serializer.DescriptionStore.GetDescription(TCollection);
      Description.ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

      Serializer.JSONStringToObject('[{"Name": "JoJo"}, {"Name":"Second name"}]', Coll);

      CheckEquals(2, Coll.Count, 'JSONToObject return an invalid amount of items in the collection');
      Check(Assigned(Coll.Items[0]), 'First item in the collection is not assigned?');
      Check(Coll.Items[0] is TNamedItem, 'First item in the collection is not a TNamedItem');
      CheckEquals('JoJo', TNamedItem(Coll.Items[0]).Name, 'Name of first item should be nil, because there is no description given for the elements');

      Check(Assigned(Coll.Items[1]), 'Second item in the collection is not assigned?');
      Check(Coll.Items[1] is TNamedItem, 'Second item in the collection is not a TNamedItem');
      CheckEquals('Second name', TNamedItem(Coll.Items[1]).Name, 'Name of second item should be nil, because there is no description given for the elements');
    finally
      Coll.Free;
    end;
  finally
    Serializer.Free;
  end;

end;

procedure TSerializeCollectionTests.TestJSONArrayToCollectionAsProperty;
var
  Inst: TTestCollectionProperty;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [tcsdfCollectionAsList];

    Inst := TTestCollectionProperty.Create;
    try
      Description := Serializer.DescriptionStore.GetDescription(TTestCollectionProperty);
      Description.Properties.FindByPropertyName('Coll').ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

      Serializer.JSONStringToObject('{ "Coll" : [{"Name": "JoJo"}, {"Name":"Second name"}]}', Inst);

      CheckEquals(2, Inst.Coll.Count, 'JSONToObject return an invalid amount of items in the collection');
      Check(Assigned(Inst.Coll.Items[0]), 'First item in the collection is not assigned?');
      Check(Inst.Coll.Items[0] is TNamedItem, 'First item in the collection is not a TNamedItem');
      CheckEquals('JoJo', TNamedItem(Inst.Coll.Items[0]).Name, 'Name of first item should be nil, because there is no description given for the elements');

      Check(Assigned(Inst.Coll.Items[1]), 'Second item in the collection is not assigned?');
      Check(Inst.Coll.Items[1] is TNamedItem, 'Second item in the collection is not a TNamedItem');
      CheckEquals('Second name', TNamedItem(Inst.Coll.Items[1]).Name, 'Name of second item should be nil, because there is no description given for the elements');
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TSerializeCollectionTests.TestJSONToCollectionAsProperty;
var
  Inst: TTestCollectionProperty;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [];
  Serializer := TJSONRttiStreamClass.Create;
  try
    Inst := TTestCollectionProperty.Create;
    try
      Description := Serializer.DescriptionStore.GetDescription(TTestCollectionProperty);
      Description.Properties.FindByPropertyName('Coll').Properties.FindByPropertyName('Items').ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;

      Serializer.JSONStringToObject('{ "Coll" : { "Items" : [{"Name": "JoJo"}, {"Name":"Second name"}] } }', Inst);

      CheckEquals(2, Inst.Coll.Count, 'JSONToObject return an invalid amount of items in the collection');
      Check(Assigned(Inst.Coll.Items[0]), 'First item in the collection is not assigned?');
      Check(Inst.Coll.Items[0] is TNamedItem, 'First item in the collection is not a TNamedItem');
      CheckEquals('JoJo', TNamedItem(Inst.Coll.Items[0]).Name, 'Name of first item should be nil, because there is no description given for the elements');

      Check(Assigned(Inst.Coll.Items[1]), 'Second item in the collection is not assigned?');
      Check(Inst.Coll.Items[1] is TNamedItem, 'Second item in the collection is not a TNamedItem');
      CheckEquals('Second name', TNamedItem(Inst.Coll.Items[1]).Name, 'Name of second item should be nil, because there is no description given for the elements');
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

{ TDescriptionTests }

procedure TDescriptionTests.TestExportNameStyle;
var
  Inst: TTestObject;
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Inst := TTestObject.Create;
    try
      Inst.Prop1:='prop1';
      Inst.Prop2:='prop2';
      Inst.Int1:=54321;
      Inst.CamelCaseProp:=3524;

      Description := Serializer.DescriptionStore.GetDescription(TTestObject);
      Description.Properties.FindByPropertyName('Prop1').ExportNameStyle := tcsensLowerCase;
      Description.Properties.FindByPropertyName('Prop2').ExportNameStyle := tcsensLiteral;
      Description.Properties.FindByPropertyName('Int1').ExportNameStyle := tcsensUpperCase;
      Description.Properties.FindByPropertyName('CamelCaseProp').ExportNameStyle := tcsensLowerCaseFirstChar;

      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        Check(Assigned(JS.Find('prop1')), 'ObjectToJson did not add the property ''prop1''');
        Check(Assigned(JS.Find('Prop2')), 'ObjectToJson did not add the property ''prop2''');
        Check(Assigned(JS.Find('INT1')), 'ObjectToJson did not add the property ''int11''' + js.AsJSON);
        Check(Assigned(JS.Find('camelCaseProp')), 'ObjectToJson did not add the property ''camelCaseProp''' + js.AsJSON);

        CheckEquals('prop1', JS.Get('prop1', ''), 'Property 1');
        CheckEquals('prop2', JS.Get('Prop2', ''), 'Property 2');
        CheckEquals(54321, JS.Get('INT1', -1), 'Integer Property 1');
        CheckEquals(3524, JS.Get('camelCaseProp', 01), 'CamelCaseProp 1');
      finally
        JS.Free;
      end;
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TDescriptionTests.TestParentExportNameStyle;
var
  Inst: TTestObject;
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Inst := TTestObject.Create;
    try
      Inst.Prop1:='prop1';
      Inst.Prop2:='prop2';
      Inst.Int1:=54321;
      Inst.CamelCaseProp:=3524;

      Description := Serializer.DescriptionStore.GetDescription(TTestObject);
      Description.ExportNameStyle := tcsensLowerCase;
      Description.Properties.FindByPropertyName('CamelCaseProp').ExportNameStyle := tcsensLowerCaseFirstChar;

      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        Check(Assigned(JS.Find('prop1')), 'ObjectToJson did not add the property ''prop1''');
        Check(Assigned(JS.Find('prop2')), 'ObjectToJson did not add the property ''prop2''');
        Check(Assigned(JS.Find('int1')), 'ObjectToJson did not add the property ''int11''' + js.AsJSON);
        Check(Assigned(JS.Find('camelCaseProp')), 'ObjectToJson did not add the property ''camelCaseProp''' + js.AsJSON);

        CheckEquals('prop1', JS.Get('prop1', ''), 'Property 1');
        CheckEquals('prop2', JS.Get('prop2', ''), 'Property 2');
        CheckEquals(54321, JS.Get('int1', -1), 'Integer Property 1');
        CheckEquals(3524, JS.Get('camelCaseProp', 01), 'CamelCaseProp 1');
      finally
        JS.Free;
      end;
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TDescriptionTests.TestClone;
var
  Inst: TTestObject;
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Inst := TTestObject.Create;
    try
      Inst.Prop1:='prop1';
      Inst.Prop2:='prop2';
      Inst.Int1:=54321;
      Inst.CamelCaseProp:=3524;

      Description := Serializer.DescriptionStore.GetDescription(TTestObject);
      Description.Properties.FindByPropertyName('Prop1').ExportNameStyle := tcsensLowerCase;
      Description := Serializer.DescriptionStore.CloneDescription(TTestObject, '', 'upper');
      Description.Properties.FindByPropertyName('Prop1').ExportNameStyle := tcsensUpperCase;

      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try
        Check(Assigned(JS.Find('prop1')), 'ObjectToJson did not add the property ''prop1''');
        Check(Assigned(JS.Find('Prop2')), 'ObjectToJson did not add the property ''prop2''');
        Check(Assigned(JS.Find('Int1')), 'ObjectToJson did not add the property ''int11''' + js.AsJSON);
        Check(Assigned(JS.Find('CamelCaseProp')), 'ObjectToJson did not add the property ''camelCaseProp''' + js.AsJSON);

        CheckEquals('prop1', JS.Get('prop1', ''), 'Property 1');
        CheckEquals('prop2', JS.Get('Prop2', ''), 'Property 2');
        CheckEquals(54321, JS.Get('Int1', -1), 'Integer Property 1');
        CheckEquals(3524, JS.Get('CamelCaseProp', 01), 'CamelCaseProp 1');
      finally
        JS.Free;
      end;

      JS := Serializer.ObjectToJSon(Inst, 'upper') as TJSONObject;
      try
        Check(Assigned(JS.Find('PROP1')), 'ObjectToJson did not add the property ''prop1''');
        Check(Assigned(JS.Find('Prop2')), 'ObjectToJson did not add the property ''prop2''');
        Check(Assigned(JS.Find('Int1')), 'ObjectToJson did not add the property ''int11''' + js.AsJSON);
        Check(Assigned(JS.Find('CamelCaseProp')), 'ObjectToJson did not add the property ''camelCaseProp''' + js.AsJSON);

        CheckEquals('prop1', JS.Get('PROP1', ''), 'Property 1');
        CheckEquals('prop2', JS.Get('Prop2', ''), 'Property 2');
        CheckEquals(54321, JS.Get('Int1', -1), 'Integer Property 1');
        CheckEquals(3524, JS.Get('CamelCaseProp', 01), 'CamelCaseProp 1');
      finally
        JS.Free;
      end;

    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TDescriptionTests.TestDefaultExportNameStyle;
var
  Inst: TTestObject;
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Serializer.DescriptionStore.Describer.DefaultExportNameStyle := tcsensLowerCase;

    Inst := TTestObject.Create;
    try
      Inst.Prop1:='prop1';
      Inst.Prop2:='prop2';
      Inst.Int1:=54321;
      Inst.CamelCaseProp:=3524;

      Description := Serializer.DescriptionStore.GetDescription(TTestObject);
      Check(Description.ExportNameStyle = tcsensLowerCase, 'The ExportNameStyle for the description has not been set to the default');
      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        Check(Assigned(JS.Find('prop1')), 'ObjectToJson did not add the property ''prop1''');
        Check(Assigned(JS.Find('prop2')), 'ObjectToJson did not add the property ''prop2''');
        Check(Assigned(JS.Find('int1')), 'ObjectToJson did not add the property ''int11''' + js.AsJSON);
        Check(Assigned(JS.Find('camelcaseprop')), 'ObjectToJson did not add the property ''camelCaseProp''' + js.AsJSON);

        CheckEquals('prop1', JS.Get('prop1', ''), 'Property 1');
        CheckEquals('prop2', JS.Get('prop2', ''), 'Property 2');
        CheckEquals(54321, JS.Get('int1', -1), 'Integer Property 1');
        CheckEquals(3524, JS.Get('camelcaseprop', 01), 'CamelCaseProp 1');
      finally
        JS.Free;
      end;
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TDescriptionTests.TestImportNameStyle;
var
  Inst: TTestObject;
  JS: TJSONData;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Description := Serializer.DescriptionStore.GetDescription(TTestObject);
    Description.ImportNameStyle := tcsinsLowercase;
    Description.Properties.FindByPropertyName('Prop2').ImportNameStyle := tcsinsCaseSensitive;
    Description.Properties.FindByPropertyName('Int1').ImportNameStyle := tcsinsUpperCase;
    Description.Properties.FindByPropertyName('CamelCaseProp').ImportNameStyle := tcsinsLowerCaseFirstChar;

    JS := GetJSON('{"prop1":"Val1","Prop2":"Val2","INT1":42,"camelCaseProp":23}');
    try
      Inst := TTestObject.Create;
      try
        Serializer.JSONToObject(JS as TJSONObject, Inst);

        CheckEquals('Val1', Inst.Prop1, 'JSONToObject did not get the property ''prop1''');
        CheckEquals('Val2', Inst.Prop2, 'JSONToObject did not get the property ''prop2''');
        CheckEquals(42, Inst.Int1, 'JSONToObject did not get the property ''Int1''');
        CheckEquals(23, Inst.CamelCaseProp, 'JSONToObject did not get the property ''CamelCaseProp''');
      finally
        Inst.Free;
      end;
    finally
      Js.Free;
    end;

    Description := Serializer.DescriptionStore.GetDescription(TTestObject);
    Description.Properties.FindByPropertyName('Prop1').ImportNameStyle := tcsinsCaseInsensitive;
    Description.Properties.FindByPropertyName('Prop2').ImportNameStyle := tcsinsCaseSensitive;

    JS := GetJSON('{"pRoP1":"Val1","prop2":"Val2"}');
    try
      Inst := TTestObject.Create;
      try
        Serializer.JSONToObject(JS as TJSONObject, Inst);
        CheckEquals('Val1', Inst.Prop1, 'JSONToObject did not get the property ''prop1''');
        CheckEquals('', Inst.Prop2, 'JSONToObject got a value for ''prop1'' which it should not have because the case of the propertyname does not match');
      finally
        Inst.Free;
      end;
    finally
      Js.Free;
    end;

  finally
    Serializer.Free;
  end;
end;

procedure TDescriptionTests.TestEnumerationOnGetAsString;
var
  Inst: TTestEnumObject;
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Inst := TTestEnumObject.Create;
    try
      Inst.EnumProp1:=Enum2;
      Inst.EnumProp2:=Enum4;

      Description := Serializer.DescriptionStore.GetDescription(TTestEnumObject);
      Description.Properties.FindByPropertyName('EnumProp1').OnGetValueAsString := @GetEnumValueAsString;
      Description.Properties.FindByPropertyName('EnumProp2').OnGetValueAsString := @GetEnumValueAsString;

      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try
        TestJSONProperty(JS, 'EnumProp1', jtString, 'E2');
        TestJSONProperty(JS, 'EnumProp2', jtString, 'Enum4');
      finally
        JS.Free;
      end;
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

procedure TDescriptionTests.TestEnumerationOnSetAsString;
var
  Inst: TTestEnumObject;
  JS: TJSONData;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Description := Serializer.DescriptionStore.GetDescription(TTestEnumObject);
    Description.Properties.FindByPropertyName('EnumProp1').OnSetValueAsString := @SetEnumValueAsString;
    Description.Properties.FindByPropertyName('EnumProp2').OnSetValueAsString := @SetEnumValueAsString;

    JS := GetJSON('{"EnumProp1":"E2","EnumProp2":"Enum4"}');
    try
      Inst := TTestEnumObject.Create;
      try
        Serializer.JSONToObject(JS as TJSONObject, Inst);
        Check(enum2=Inst.EnumProp1, 'JSONToObject did not get the property ''Enumprop1''');
        Check(Enum4=Inst.EnumProp2, 'JSONToObject did not get the property ''Enumprop2''');
      finally
        Inst.Free;
      end;
    finally
      Js.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

function TDescriptionTests.GetEnumValueAsString(AnInstance: TObject; ADescription: TcsStreamDescription; out AValue: string): boolean;
begin
  if ADescription.PropertyName='EnumProp1' then
    begin
    if TTestEnumObject(AnInstance).EnumProp1 = Enum2 then
      AValue := 'E2'
    else
      AValue := 'Invaid property value?';
    Result := True;
    end
  else
    // Use the default way to obtain the value.
    Result := False;
end;

function TDescriptionTests.SetEnumValueAsString(AnInstance: TObject; ADescription: TcsStreamDescription; const AValue: string): boolean;
begin
  if ADescription.PropertyName='EnumProp1' then
    begin
    if AValue = 'E2' then
      (AnInstance as TTestEnumObject).EnumProp1 := Enum2
    else
      (AnInstance as TTestEnumObject).EnumProp1 := Enum1;
    Result := True;
    end
  else
    // Use the default way to obtain the value.
    Result := False;
end;

procedure TDescriptionTests.TestParentOfSubClasses;
var
  Inst: TTestSubObjectObject;
  JS: TJSONData;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Description := Serializer.DescriptionStore.GetDescription(TTestSubObjectObject);
    Description.ImportNameStyle := tcsinsLowercase;

    JS := GetJSON('{"obj3":{"prop1":"property value 1"}}');
    try
      Inst := TTestSubObjectObject.Create;
      try
        Serializer.JSONToObject(JS as TJSONObject, Inst);

        CheckEquals('property value 1', Inst.Obj3.Prop1, 'JSONToObject did not set the property ''obj1.prop1''');
      finally
        Inst.Free;
      end;
    finally
      Js.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

{ TSerializeArraysInObjectTests }


procedure TSerializeArraysInObjectTests.TestSimpleArrayToJSON;
var
  Inst: TTestArrayObject;
  JS: TJSONObject;
  JSProp: TJSONData;
begin
  Inst := TTestArrayObject.Create;
  try
    Inst.Prop1:='prop1';
    Inst.ArrOfInt := [1,5,634];

    JS := TJSONRttiStreamHelper.ObjectToJSon(Inst) as TJSONObject;
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      CheckEquals('prop1', JS.Get('Prop1', ''), 'Property 1');

      JSProp := JS.Find('ArrOfInt');
      Check(Assigned(JSProp), 'ObjectToJson did not add the property ''ArrOfInt''');
      Check(jtArray = JSProp.JSONType, 'Array of integer was not converted into a json-array');
      CheckEquals(3, TJSONArray(JSProp).Count, 'The JSON-Array does not have the correct amount of elements');
      Check(jtNumber = TJSONArray(JSProp).Items[0].JSONType, 'Elements of the array do not have the correct json-type');
      CheckEquals(1, TJSONArray(JSProp).Items[0].AsInteger, 'The elemnt of the JSON-array does not have the correct value');
      CheckEquals(5, TJSONArray(JSProp).Items[1].AsInteger, 'The elemnt of the JSON-array does not have the correct value');
      CheckEquals(634, TJSONArray(JSProp).Items[2].AsInteger, 'The elemnt of the JSON-array does not have the correct value');
    finally
      JS.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializeArraysInObjectTests.TestSimpleStringArrayToJSON;
var
  Inst: TTestStringArrayObject;
  JS: TJSONObject;
  JSProp: TJSONData;
begin
  Inst := TTestStringArrayObject.Create;
  try
    Inst.Prop1:='prop1';
    Inst.ArrOfStr := ['string1','ele2','not empty'];

    JS := TJSONRttiStreamHelper.ObjectToJSon(Inst) as TJSONObject;
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      CheckEquals('prop1', JS.Get('Prop1', ''), 'Property 1');

      JSProp := JS.Find('ArrOfStr');
      Check(Assigned(JSProp), 'ObjectToJson did not add the property ''ArrOfInt''');
      Check(jtArray = JSProp.JSONType, 'Array of string was not converted into a json-array');
      CheckEquals(3, TJSONArray(JSProp).Count, 'The JSON-Array does not have the correct amount of elements');
      Check(jtString = TJSONArray(JSProp).Items[0].JSONType, 'Elements of the array do not have the correct json-type');
      CheckEquals('string1', TJSONArray(JSProp).Items[0].AsString, 'The elemnt of the JSON-array does not have the correct value');
      CheckEquals('ele2', TJSONArray(JSProp).Items[1].AsString, 'The elemnt of the JSON-array does not have the correct value');
      CheckEquals('not empty', TJSONArray(JSProp).Items[2].AsString, 'The elemnt of the JSON-array does not have the correct value');
    finally
      JS.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializeArraysInObjectTests.TestJSONStringToSimpleArray;
var
  Inst: TTestArrayObject;
begin
  Inst := TTestArrayObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"Prop1":"Val1","ArrOfInt":[1,75,235]}', Inst);

    CheckEquals('Val1', Inst.Prop1, 'JSONToObject did not get the property ''prop1''');
    CheckEquals(3, Length(Inst.ArrOfInt), 'JSONToObject did not get the element count of the array right');
    CheckEquals(1, Inst.ArrOfInt[0], 'JSONToObject did not get the value of the first element right');
    CheckEquals(75, Inst.ArrOfInt[1], 'JSONToObject did not get the value of the second element right');
    CheckEquals(235, Inst.ArrOfInt[2], 'JSONToObject did not get the value of the third element right');
  finally
    Inst.Free;
  end;
end;

procedure TSerializeArraysInObjectTests.TestJSONStringToSimpleStringArray;
var
  Inst: TTestStringArrayObject;
begin
  Inst := TTestStringArrayObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"Prop1":"Val1","ArrOfStr":["valt","","TaDaa"]}', Inst);

    CheckEquals('Val1', Inst.Prop1, 'JSONToObject did not get the property ''prop1''');
    CheckEquals(3, Length(Inst.ArrOfStr), 'JSONToObject did not get the element count of the array right');
    CheckEquals('valt', Inst.ArrOfStr[0], 'JSONToObject did not get the value of the first element right');
    CheckEquals('', Inst.ArrOfStr[1], 'JSONToObject did not get the value of the second element right');
    CheckEquals('TaDaa', Inst.ArrOfStr[2], 'JSONToObject did not get the value of the third element right');
  finally
    Inst.Free;
  end;
end;


procedure TSerializeSimpleObjectTests.TestSimpleClassToJSON;
var
  Inst: TTestObject;
  JS: TJSONObject;
begin
  Inst := TTestObject.Create;
  try
    Inst.Prop1:='prop1';
    Inst.Prop2:='prop2';
    Inst.Int1:=54321;

    JS := TJSONRttiStreamHelper.ObjectToJSon(Inst) as TJSONObject;
    try
      Check(Assigned(JS), 'ObjectToJSon returned nil');
      Check(Assigned(JS.Find('Prop1')), 'ObjectToJson did not add the property ''prop1''');
      Check(Assigned(JS.Find('Prop2')), 'ObjectToJson did not add the property ''prop1''');
      Check(Assigned(JS.Find('Int1')), 'ObjectToJson did not add the property ''int11''' + js.AsJSON);

      CheckEquals('prop1', JS.Get('Prop1', ''), 'Property 1');
      CheckEquals('prop2', JS.Get('Prop2', ''), 'Property 2');
      CheckEquals(54321, JS.Get('Int1', -1), 'Integer Property 1');
    finally
      JS.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializeSimpleObjectTests.TestJSONToSimpleClass;
var
  Inst: TTestObject;
  JS: TJSONData;
begin
  JS := GetJSON('{"Prop1":"Val1","Prop2":"Val2","Int1":42}');
  try
    Inst := TTestObject.Create;
    try
      TJSONRttiStreamHelper.JSONToObject(JS as TJSONObject, Inst);

      CheckEquals('Val1', Inst.Prop1, 'JSONToObject did not get the property ''prop1''');
      CheckEquals('Val2', Inst.Prop2, 'JSONToObject did not get the property ''prop2''');
      CheckEquals(42, Inst.Int1, 'JSONToObject did not get the property ''Int1''');
    finally
      Inst.Free;
    end;
  finally
    Js.Free;
  end;
end;

procedure TSerializeSimpleObjectTests.TestJSONStringToSimpleClass;
var
  Inst: TTestObject;
begin
  Inst := TTestObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"Prop1":"Val1","Prop2":"Val2","Int1":42}', Inst);

    CheckEquals('Val1', Inst.Prop1, 'JSONToObject did not get the property ''prop1''');
    CheckEquals('Val2', Inst.Prop2, 'JSONToObject did not get the property ''prop2''');
    CheckEquals(42, Inst.Int1, 'JSONToObject did not get the property ''Int1''');
  finally
    Inst.Free;
  end;
end;

procedure TSerializeSimpleObjectTests.TestEnumeration;
var
  Inst: TTestEnumObject;
  JSObject: TJSONObject;
begin
  Inst := TTestEnumObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"EnumProp1":"Enum3","EnumProp2":"Enum1"}', Inst);

    Check(Enum3=Inst.EnumProp1, 'JSONToObject did not get the first enumeration-property right');
    Check(Enum1=Inst.EnumProp2, 'JSONToObject did not get the second enumeration-property right');

    JSObject := TJSONRttiStreamHelper.ObjectToJSON(Inst) as TJSONObject;
    try
      TestJSONProperty(JSObject, 'EnumProp1', jtString, 'Enum3');
      TestJSONProperty(JSObject, 'EnumProp2', jtString, 'Enum1');
    finally
      JSObject.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializeSimpleObjectTests.TestDatetime;
var
  Inst: TTestDatetimeObject;
  JSObject: TJSONObject;
begin
  Inst := TTestDatetimeObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"DatetimeProp1":"2020-04-23T12:34:32","DateProp1":"2019-02-03"}', Inst);

    Check(CompareValue(ComposeDateTime(EncodeDate(2020,4,23), EncodeTime(12, 34, 32, 00)), Inst.DatetimeProp1)=EqualsValue, 'JSONToObject did not get the first datetime-property right');
    Check(CompareValue(EncodeDate(2019,2,3), Inst.DateProp1)=EqualsValue, 'JSONToObject did not get the first date-property right');

    Inst.DatetimeProp1 := ComposeDateTime(EncodeDate(2020,2,17), EncodeTime(23,12,4,234));
    inst.DateProp1 := EncodeDate(2020,2,16);

    JSObject := TJSONRttiStreamHelper.ObjectToJSON(Inst) as TJSONObject;
    try
      TestJSONProperty(JSObject, 'DatetimeProp1', jtString, '2020-02-17T23:12:04.234');
      TestJSONProperty(JSObject, 'DatetimeProp2', jtNull);

      TestJSONProperty(JSObject, 'DateProp1', jtString, '2020-02-16');
      TestJSONProperty(JSObject, 'DateProp2', jtNull);
    finally
      JSObject.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializeSimpleObjectTests.TestFloat;
var
  Inst: TTestFloatObject;
  JSObject: TJSONObject;
begin
  Inst := TTestFloatObject.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"FloatProp1":-4,"FloatProp2":1.2}', Inst);

    CheckEquals(-4, Inst.FloatProp1, 'JSONToObject did not get the first float-property right');
    CheckEquals(1.2, Inst.FloatProp2, 'JSONToObject did not get the second float-property right');

    JSObject := TJSONRttiStreamHelper.ObjectToJSON(Inst) as TJSONObject;
    try
      TestJSONProperty(JSObject, 'FloatProp1', jtNumber, -4);
      TestJSONProperty(JSObject, 'FloatProp2', jtNumber, 1.2);
    finally
      JSObject.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TSerializeSimpleObjectTests.TestBoolean;
var
  Inst: TTestBooleanProperty;
  JSObject: TJSONObject;
begin
  Inst := TTestBooleanProperty.Create;
  try
    TJSONRttiStreamHelper.JSONStringToObject('{"Bool1":true,"Bool2":false}', Inst);

    CheckEquals(True, Inst.Bool1, 'JSONToObject did not get the first boolean-property right');
    CheckEquals(False, Inst.Bool2, 'JSONToObject did not get the second boolean-property right');

    JSObject := TJSONRttiStreamHelper.ObjectToJSON(Inst) as TJSONObject;
    try
      TestJSONProperty(JSObject, 'Bool1', jtBoolean, True);
      TestJSONProperty(JSObject, 'Bool2', jtBoolean, False);
    finally
      JSObject.Free;
    end;
  finally
    Inst.Free;
  end;
end;

procedure TCustomSerializeTest.TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: Integer);
var
  JSValue: TJSONData;
begin
  JSValue := TestJSONProperty(AJSONObject, APropName, AJSonType);
  CheckEquals(Value, JSValue.AsInteger, Format('Property [%s]', [APropName]));
end;

procedure TCustomSerializeTest.TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: string);
var
  JSValue: TJSONData;
begin
  JSValue := TestJSONProperty(AJSONObject, APropName, AJSonType);
  CheckEquals(Value, JSValue.AsString, Format('Property [%s]', [APropName]));
end;

function TCustomSerializeTest.TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype): TJSONData;
begin
  Result := AJSONObject.Find(APropName);
  Check(Assigned(Result), Format('No JSON-value found for property [%s]', [APropName]));
  Check(Result.JSONType=AJSonType, Format('The value for property [%s] is of JSON-type [%s] instead of [%s]', [APropName, GetEnumName(TypeInfo(TJSONtype), Ord(Result.JSONType)), GetEnumName(TypeInfo(TJSONtype), Ord(AJSonType))]));
  Check(Result.JSONType=AJSonType, 'test');
end;

procedure TCustomSerializeTest.TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: Extended);
var
  JSValue: TJSONData;
begin
  JSValue := TestJSONProperty(AJSONObject, APropName, AJSonType);
  CheckEquals(Value, JSValue.AsFloat, Format('Property [%s]', [APropName]));
end;

procedure TCustomSerializeTest.TestJSONProperty(AJSONObject: TJSONObject; APropName: string; AJSonType: TJSONtype; Value: Boolean);
var
  JSValue: TJSONData;
begin
  JSValue := TestJSONProperty(AJSONObject, APropName, AJSonType);
  CheckEquals(Value, JSValue.AsBoolean, Format('Property [%s]', [APropName]));
end;

{ TSerializeFilterTests }

procedure TSerializeFilterTests.TestPropertyFilter;
var
  Inst: TTestObject;
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
begin
  Serializer := TJSONRttiStreamClass.Create;
  try
    Serializer.DescriptionStore.GetDescription(TTestObject).Properties.FindByPropertyName('Prop2').OnFilter := @DoFilterProp;
    Inst := TTestObject.Create;
    try
      Inst.Prop1:='prop1';
      Inst.Prop2:='SkipMe';
      Inst.Int1:=54321;

      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        TestJSONProperty(JS, 'Prop1', jtString, 'prop1');
        TestJSONProperty(JS, 'Int1', jtNumber, 54321);
        Check(not Assigned(JS.Find('prop2')), 'ObjectToJson did should have skipped ''prop2''');
      finally
        JS.Free;
      end;

      Inst.Prop2:='Do not SkipMe';
      JS := Serializer.ObjectToJSon(Inst) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        TestJSONProperty(JS, 'Prop1', jtString, 'prop1');
        TestJSONProperty(JS, 'Prop2', jtString, 'Do not SkipMe');
        TestJSONProperty(JS, 'Int1', jtNumber, 54321);
      finally
        JS.Free;
      end;
    finally
      Inst.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

function TSerializeFilterTests.DoFilterProp(AnInstance: TObject; ADescription: TcsStreamDescription): boolean;
begin
  Result := TTestObject(AnInstance).Prop2 <> 'SkipMe';
end;

procedure TSerializeFilterTests.TestListFilter;
var
  JS: TJSONObject;
  Serializer: TJSONRttiStreamClass;
  Description: TcsStreamDescription;
  Coll: TCollection;
  Item: TNamedItem;
  JSArray: TJSONArray;
begin
  (TcsClassDescriberFactory.Instance.GetClassDescriber(TcsCollectionDescriber) as TcsCollectionDescriber).Flags := [];
  Serializer := TJSONRttiStreamClass.Create;
  try
    Coll := TCollection.Create(TNamedItem);
    try
      Item := TNamedItem.Create(Coll);
      Item.Name := 'One of the items';
      Item := TNamedItem.Create(Coll);
      Item.Name := 'Skipped item';
      Item := TNamedItem.Create(Coll);
      Item.Name := 'Another one of the items';

      Description := Serializer.DescriptionStore.GetDescription(TCollection);
      Description.Properties.FindByPropertyName('Items').ListDescription.DefaultSubObjectDescription := Serializer.DescriptionStore.GetDescription(TNamedItem).Clone;
      Description.Properties.FindByPropertyName('Items').ListDescription.OnFilter := @DoListFilterProp;

      JS := Serializer.ObjectToJSon(Coll) as TJSONObject;
      try
        Check(Assigned(JS), 'ObjectToJSon returned nil');
        JSArray := TestJSONProperty(JS, 'Items', jtArray) as TJSONArray;
        CheckEquals(2, JSArray.Count, 'The amount of items in the collection and the resulting json-array do not match');

        Check(JSArray.Items[0].JSONType = jtObject, 'The first item in the JSON-array does not contain an object');
        Check(JSArray.Items[1].JSONType = jtObject, 'The second item in the JSON-array does not contain an object');

        TestJSONProperty(TJSONObject(JSArray.Items[0]), 'Name', jtString, 'One of the items');
        TestJSONProperty(TJSONObject(JSArray.Items[1]), 'Name', jtString, 'Another one of the items');
      finally
        JS.Free;
      end;
    finally
      Coll.Free;
    end;
  finally
    Serializer.Free;
  end;
end;

function TSerializeFilterTests.DoListFilterProp(AnInstance: TObject; ADescription: TcsStreamDescription): boolean;
begin
  Result := ((AnInstance as TCollection).Items[ADescription.Index] AS TNamedItem).DisplayName <> 'Skipped item';
end;

initialization
  RegisterTest(TSerializeSimpleObjectTests);
  RegisterTest(TSerializeArraysInObjectTests);
  RegisterTest(TDescriptionTests);
  RegisterTest(TSerializeSubObjectsTests);
  RegisterTest(TStreamHelperFlagsTests);
  RegisterTest(TSerializeCollectionTests);
  RegisterTest(TSerializePropertiesTests);
  RegisterTest(TSerializeFilterTests);
  RegisterTest(TClassDescriptorFactoryTests);
end.

