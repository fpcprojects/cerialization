unit csStream;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  csModel;

type

  { TcsModelStream }

  TcsModelStream = class(TcsLogBaseClass)
  protected
    function FilterElement(AnInstance: TObject; ADescription: TcsStreamDescription): Boolean;
    function FormatName(const AName: string; ExportNameStype: TcsExportNameStyle): string; overload;
    function FormatName(const AName: string; ImportNameStype: TcsImportNameStyle): string; overload;
  public
    function ObjectToModel(AnInstance: TObject; AStreamDescription: TcsStreamDescription): TObject; virtual; abstract;
    procedure ModelToObject(AModel: TObject; AnInstance: TObject; AStreamDescription: TcsStreamDescription); virtual; abstract;

    procedure GetProperty(AnInstance: TObject; AModel: TObject; AProperty: TcsStreamDescription); virtual; abstract;

    procedure PrepareItemDescriptionForIndex(const AListItemDescription, ADescription: TcsStreamDescription; const AnInstance: TObject; const Index: Integer); virtual;
  end;

  { TcsStream }

  TcsStream = class
  private
    FStreamDescription: TcsStreamDescription;
    FModelStream: TcsModelStream;
  public
    constructor Create(AStreamDescription: TcsStreamDescription; AModelStream: TcsModelStream);
    function ObjectToModel(AnInstance: TObject): TObject;
    procedure ModelToObject(AModel: TObject; AnInstance: TObject);
    property StreamDescription: TcsStreamDescription read FStreamDescription;
    property ModelStream: TcsModelStream read FModelStream;
  end;

implementation

{ TcsModelStream }

function TcsModelStream.FormatName(const AName: string; ExportNameStype: TcsExportNameStyle): string;
begin
  case ExportNameStype of
    tcsensNotSet,
    tcsensLiteral:
      Result := AName;
    tcsensLowerCase:
      Result := LowerCase(AName);
    tcsensUpperCase:
      Result := UpperCase(AName);
    tcsensLowerCaseFirstChar:
      begin
      Result := AName;
      if Result<>'' then
        Result[1] := LowerCase(Result[1]);
      end;
  end;
end;

procedure TcsModelStream.PrepareItemDescriptionForIndex(const AListItemDescription, ADescription: TcsStreamDescription; const AnInstance: TObject; const Index: Integer);
begin
  AListItemDescription.Index := Index;
end;

function TcsModelStream.FormatName(const AName: string; ImportNameStype: TcsImportNameStyle): string;
begin
  case ImportNameStype of
    tcsinsNotSet,
    tcsinsCaseSensitive:
      Result := AName;
    tcsinsLowerCase:
      Result := LowerCase(AName);
    tcsinsUpperCase:
      Result := UpperCase(AName);
    tcsinsLowerCaseFirstChar:
      begin
      Result := AName;
      if Result<>'' then
        Result[1] := LowerCase(Result[1]);
      end;
  end;
end;

function TcsModelStream.FilterElement(AnInstance: TObject; ADescription: TcsStreamDescription): Boolean;
begin
  if Assigned(ADescription.OnFilter) then
    Result := ADescription.OnFilter(AnInstance, ADescription)
  else
    Result := True;
end;

{ csStream }

constructor TcsStream.Create(AStreamDescription: TcsStreamDescription; AModelStream: TcsModelStream);
begin
  FStreamDescription := AStreamDescription;
  FModelStream := AModelStream;
end;

function TcsStream.ObjectToModel(AnInstance: TObject): TObject;
begin
  Result := FModelStream.ObjectToModel(AnInstance, FStreamDescription);
end;

procedure TcsStream.ModelToObject(AModel: TObject; AnInstance: TObject);
begin
  FModelStream.ModelToObject(AModel, AnInstance, FStreamDescription);
end;

end.

