unit csJSONRttiStreamHelper;

{$mode objfpc}{$H+}

interface

uses
  fpjson,
  jsonparser,
  csStreamHelper,
  csRttiModel,
  csStream,
  csModel,
  csStreamDescriptionStore,
  csJSONStream;

type

  { TJSONRttiStreamHelper }

  TJSONRttiStreamHelper = class(TcsStreamHelper)
  protected
    class function ObtainDescriber(): TcsDescriber; override;
    class function ObtainModelStream(): TcsModelStream; override;
  public
    class function ObjectToJSON(AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []): TJSONData;
    class function ObjectToJSONString(AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []): string;

    class procedure JSONToObject(AJSON: TJSONData; AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []);
    class procedure JSONStringToObject(AJSONString: TJSONStringType; AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []);
  end;

  { TJSONRttiStreamClass }

  TJSONRttiStreamClass = class(TcsStreamClass)
  protected
    function GetStreamHelper: TcsStreamHelperClass; override;
  public
    function ObjectToJSON(AnInstance: TObject; ADescriptionTag: string = ''): TJSONData;
    function ObjectToJSONString(AnInstance: TObject; ADescriptionTag: string = ''): string;

    procedure JSONToObject(AJSON: TJSONData; AnInstance: TObject; ADescriptionTag: string = '');
    procedure JSONStringToObject(AJSONString: TJSONStringType; AnInstance: TObject; ADescriptionTag: string = '');

    generic function CreateObjectFromJSONString<T: TObject>(AJSONString: TJSONStringType; ADescriptionTag: string = ''): T;
  end;

implementation

{ TJSONRttiStreamClass }

function TJSONRttiStreamClass.GetStreamHelper: TcsStreamHelperClass;
begin
  Result := TJSONRttiStreamHelper;
end;

procedure TJSONRttiStreamClass.JSONStringToObject(AJSONString: TJSONStringType; AnInstance: TObject; ADescriptionTag: string);
begin
  TJSONRttiStreamHelper.JSONStringToObject(AJSONString, AnInstance, DescriptionStore.GetDescription(AnInstance.ClassType, ADescriptionTag));
end;

procedure TJSONRttiStreamClass.JSONToObject(AJSON: TJSONData; AnInstance: TObject; ADescriptionTag: string);
begin
  TJSONRttiStreamHelper.JSONToObject(AJSON, AnInstance, DescriptionStore.GetDescription(AnInstance.ClassType, ADescriptionTag));
end;

function TJSONRttiStreamClass.ObjectToJSON(AnInstance: TObject; ADescriptionTag: string): TJSONData;
begin
  Result := TJSONRttiStreamHelper.ObjectToJSON(AnInstance, DescriptionStore.GetDescription(AnInstance.ClassType, ADescriptionTag));
end;

function TJSONRttiStreamClass.ObjectToJSONString(AnInstance: TObject; ADescriptionTag: string): string;
begin
  Result := TJSONRttiStreamHelper.ObjectToJSONString(AnInstance, DescriptionStore.GetDescription(AnInstance.ClassType, ADescriptionTag));
end;

generic function TJSONRttiStreamClass.CreateObjectFromJSONString<T>(AJSONString: TJSONStringType; ADescriptionTag: string = ''): T;
var
  Obj: Tobject;
begin
  Obj := T.ClassType.Create();
  try
    JSONStringToObject(AJSONString, Obj, ADescriptionTag);
    Result := T(Obj);
    Obj := nil;
  finally
    Obj.Free;
  end;
end;

{ TJSONRttiStreamHelper }

class function TJSONRttiStreamHelper.ObtainDescriber(): TcsDescriber;
begin
  Result := TcsRttiDescriber.Create();
end;

class function TJSONRttiStreamHelper.ObtainModelStream(): TcsModelStream;
begin
  Result := TcsJSONStream.Create;
end;

class function TJSONRttiStreamHelper.ObjectToJSON(AnInstance: TObject; ADescription: TcsStreamDescription; ADescriberFlags: TcsDescriberFlagSet = []): TJSONData;
begin
  Result := ObjectToModel(AnInstance, ADescription, ADescriberFlags) as TJSONData;
end;

class procedure TJSONRttiStreamHelper.JSONToObject(AJSON: TJSONData; AnInstance: TObject; ADescription: TcsStreamDescription; ADescriberFlags: TcsDescriberFlagSet = []);
begin
  ModelToObject(AJSON, AnInstance, ADescription, ADescriberFlags);
end;

class function TJSONRttiStreamHelper.ObjectToJSONString(AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []): string;
var
  JSData: TJSONData;
begin
  JSData := ObjectToJSON(AnInstance, ADescription, ADescriberFlags);
  try
    if Assigned(JSData) then
      Result := JSData.AsJSON
    else
      Result := '';
  finally
    JSData.Free;
  end;
end;

class procedure TJSONRttiStreamHelper.JSONStringToObject(AJSONString: TJSONStringType; AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []);
var
  JSObject: TJSONData;
begin
  JSObject := GetJSON(AJSONString);
  try
    if Assigned(JSObject) then
      JSONToObject(JSObject, AnInstance, ADescription, ADescriberFlags);
  finally
    JSObject.Free;
  end;
end;

end.

