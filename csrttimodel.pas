unit csRttiModel;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  TypInfo,
  RttiUtils,
  csModel;

type

  { TcsRttiDescriber }

  TcsRttiDescriber = class(TcsDescriber)
  public
    function ObtainDescriptionForClass(AClass: TClass; AParent: TcsStreamDescription): TcsStreamDescription; override;
    function ObtainDescriptionForPropertyOfClass(AParent: TcsStreamDescription;AClassDescription: TcsDescriber; AClass: TClass; AProperty: PPropInfo): TcsStreamDescription;
    function ObtainDescriptionForType(AParent: TcsStreamDescription; APropType: PTypeInfo): TcsStreamDescription;
  end;

  { TcsRttiStreamDescription }

  TcsRttiStreamDescription = class(TcsStreamDescription)
  private
    FPropInfo: PPropInfo;
    FTypeInfo: PTypeInfo;
  protected
    procedure Assign(ASource: TcsStreamDescription); override;
  public
    function GetValueAsString(AnInstance: TObject): string; override;
    function GetValueAsInteger(AnInstance: TObject): Integer; override;
    function GetValueAsInt64(AnInstance: TObject): Int64; override;
    function GetValueAsFloat(AnInstance: TObject): Extended; override;
    function GetValueAsObject(AnInstance: TObject): TObject; override;
    function GetValueAsBoolean(AnInstance: TObject): Boolean; override;

    procedure SetValueAsString(AnInstance: TObject; Value: string); override;
    procedure SetValueAsInteger(AnInstance: TObject; Value: Integer); override;
    procedure SetValueAsInt64(AnInstance: TObject; Value: Int64); override;
    procedure SetValueAsFloat(AnInstance: TObject; Value: Extended); override;
    procedure SetValueAsBoolean(AnInstance: TObject; Value: Boolean); override;
    procedure SetValueAsObject(AnInstance: TObject; Value: TObject); override;

    function CreateNewInstance(AParentInstance: TObject): TObject; override;

    function GetItemCount(AnInstance: TObject): Int64; override;
    function SetItemCount(AnInstance: TObject; AValue: Integer): Boolean; override;

    property PropInfo: PPropInfo read FPropInfo write FPropInfo;
    property TypeInfo: PTypeInfo read FTypeInfo write FTypeInfo;
  end;

implementation

{ TcsRttiStreamDescription }

type
  TShortArr = array of ShortInt;
  TSmallArr = array of SmallInt;
  TIntArr = array of Integer;
  TInt64Arr = array of Int64;
  TStrArr = array of string;

function TcsRttiStreamDescription.GetValueAsString(AnInstance: TObject): string;
begin
  if Assigned(OnGetValueAsString) and OnGetValueAsString(AnInstance, Self, Result) then
    Exit;

  case FPropInfo^.PropType^.Kind of
    tkSString,
    tkAString:
      begin;
      Result := GetStrProp(AnInstance, FPropInfo)
      end;
    tkEnumeration:
      begin
      Result := GetEnumProp(AnInstance, FPropInfo)
      end;
    tkDynArray:
      begin
      if FTypeInfo^.Kind in [tkAString] then
        begin
        Result := TStrArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
        end;
      end
  else
    Raise EcsStreamException.Create('No string value available');
  end;
end;

function TcsRttiStreamDescription.GetValueAsInteger(AnInstance: TObject): Integer;
begin
  case FPropInfo^.PropType^.Kind of
    tkInt64, tkInteger, tkQWord:
      begin;
      Result := GetOrdProp(AnInstance, FPropInfo)
      end;
    tkDynArray:
      begin
      if FTypeInfo^.Kind in [tkInt64, tkInteger, tkQWord] then
        begin
        case GetTypeData(FPropInfo^.PropType)^.elSize of
          1: Result := TShortArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          2: Result := TSmallArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          4: Result := TIntArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          8: Result := TInt64Arr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
        else
          raise EcsStreamException.Create('Unsupported integer element size');
        end;
        end;
      end
  else
    Result:=inherited GetValueAsInteger(AnInstance);
  end;
end;

function TcsRttiStreamDescription.GetValueAsInt64(AnInstance: TObject): Int64;
begin
  case FPropInfo^.PropType^.Kind of
    tkInt64, tkInteger, tkQWord, tkBool:
      begin;
      Result := GetOrdProp(AnInstance, FPropInfo)
      end;
    tkDynArray:
      begin
      if FTypeInfo^.Kind in [tkInt64, tkInteger, tkQWord, tkBool] then
        begin
        case GetTypeData(FPropInfo^.PropType)^.elSize of
          1: Result := TShortArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          2: Result := TSmallArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          4: Result := TIntArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          8: Result := TInt64Arr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
        else
          raise EcsStreamException.Create('Unsupported integer element size');
        end;
        end;
      end
  else
    Result:=inherited GetValueAsInt64(AnInstance);
  end;
end;

procedure TcsRttiStreamDescription.SetValueAsString(AnInstance: TObject; Value: string);
begin
  if Assigned(OnSetValueAsString) and OnSetValueAsString(AnInstance, Self, Value) then
    Exit;

  case FPropInfo^.PropType^.Kind of
    tkAString,
    tkSString:
      begin
      SetStrProp(AnInstance, FPropInfo, Value)
      end;
    tkEnumeration:
      begin;
      SetEnumProp(AnInstance, FPropInfo, Value)
      end;
    tkDynArray:
      begin
      if FTypeInfo^.Kind in [tkAString, tkSString] then
        begin
        TStrArr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
        end;
      end
  else
    Raise EcsStreamException.Create('It is not possible to assign a string-value to this item');
  end;
end;

procedure TcsRttiStreamDescription.SetValueAsInteger(AnInstance: TObject; Value: Integer);
begin
  case FPropInfo^.PropType^.Kind of
    tkInt64, tkInteger, tkQWord:
      begin;
      SetOrdProp(AnInstance, FPropInfo, Value)
      end;
    tkDynArray:
      begin
      if FTypeInfo^.Kind in [tkInt64, tkInteger, tkQWord] then
        begin
        case GetTypeData(FPropInfo^.PropType)^.elSize of
          1: TShortArr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
          2: TSmallArr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
          4: TIntArr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
          8: TInt64Arr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
        else
          raise EcsStreamException.Create('Unsupported integer element size');
        end;
        end;
      end
  else
    inherited SetValueAsInteger(AnInstance, Value);
  end;
end;

procedure TcsRttiStreamDescription.SetValueAsInt64(AnInstance: TObject; Value: Int64);
begin
  case FPropInfo^.PropType^.Kind of
    tkInt64, tkInteger, tkbool, tkQWord:
      begin;
      SetOrdProp(AnInstance, FPropInfo, Value)
      end;
    tkDynArray:
      begin
      if FTypeInfo^.Kind in [tkInt64, tkInteger, tkQWord, tkBool] then
        begin
        case GetTypeData(FPropInfo^.PropType)^.elSize of
          1: TShortArr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
          2: TSmallArr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
          4: TIntArr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
          8: TInt64Arr(GetDynArrayProp(AnInstance, FPropInfo))[Index] := Value;
        else
          raise EcsStreamException.Create('Unsupported integer element size');
        end;
        end;
      end
  else
    inherited SetValueAsInt64(AnInstance, Value);
  end;
end;

function TcsRttiStreamDescription.GetItemCount(AnInstance: TObject): Int64;
type
  TDynArray=array of Byte;
var
  P: TDynArray;
begin
  Result := inherited GetItemCount(AnInstance);
  if Result = -1 then
    begin
    if  Assigned(FPropInfo) and Assigned(FPropInfo^.PropType) and (FPropInfo^.PropType^.Kind in [tkDynArray, tkArray]) then
      begin
      P := GetDynArrayProp(AnInstance, FPropInfo);
      if Assigned(P) then
        Result := Length(P)
      else
        Result := 0;
      end
    end;
end;

function TcsRttiStreamDescription.SetItemCount(AnInstance: TObject; AValue: Integer): Boolean;
var
  P: Pointer;
begin
  Result := inherited SetItemCount(AnInstance, AValue);
  if not Result and (FPropInfo^.PropType^.Kind in [tkDynArray]) then
    begin
    Result := True;
    p := nil;
    case GetTypeData(FPropInfo^.PropType)^.ElType2^.Kind of
      tkInt64, tkInteger, tkQWord:
        begin
        case GetTypeData(FPropInfo^.PropType)^.elSize of
          1: SetLength(TShortArr(P), AValue);
          2: SetLength(TSmallArr(P), AValue);
          4: SetLength(TIntArr(P), AValue);
          8: SetLength(TInt64Arr(P), AValue);
        else
          raise EcsStreamException.Create('Unsupported integer element size');
        end;
        try
          SetDynArrayProp(AnInstance, FPropInfo, P);
        finally
          SetLength(TShortArr(P), 0);
        end;
        end;
      tkSString,
      tkAString:
        begin
        SetLength(TStrArr(P), AValue);
        try
          SetDynArrayProp(AnInstance, FPropInfo, P);
        finally
          SetLength(TStrArr(P), 0);
        end;
        end;
    else
      raise EcsStreamException.Create('Unsupported element type');
    end;
    end
end;

function TcsRttiStreamDescription.GetValueAsObject(AnInstance: TObject): TObject;
begin
  Result:=inherited GetValueAsObject(AnInstance);
  if not Assigned(Result) then
    case FPropInfo^.PropType^.Kind of
      tkClass: Result := GetObjectProp(AnInstance, FPropInfo)
    end;
end;

procedure TcsRttiStreamDescription.Assign(ASource: TcsStreamDescription);
begin
  inherited Assign(ASource);
  PropInfo := TcsRttiStreamDescription(ASource).PropInfo;
  TypeInfo := TcsRttiStreamDescription(ASource).TypeInfo;
end;

procedure TcsRttiStreamDescription.SetValueAsFloat(AnInstance: TObject; Value: Extended);
begin
  if FPropInfo^.PropType^.Kind = tkFloat then
    SetFloatProp(AnInstance, FPropInfo, Value)
  else
    inherited SetValueAsFloat(AnInstance, Value);
end;

function TcsRttiStreamDescription.GetValueAsFloat(AnInstance: TObject): Extended;
begin
  case FPropInfo^.PropType^.Kind of
    tkFloat:
      begin;
      Result := GetFloatProp(AnInstance, FPropInfo)
      end;
    tkDynArray:
      begin
      if FTypeInfo^.Kind in [tkInt64, tkInteger, tkQWord] then
        begin
        case GetTypeData(FPropInfo^.PropType)^.elSize of
          1: Result := TShortArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          2: Result := TSmallArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          4: Result := TIntArr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
          8: Result := TInt64Arr(GetDynArrayProp(AnInstance, FPropInfo))[Index];
        else
          raise EcsStreamException.Create('Unsupported integer element size');
        end;
        end;
      end
  else
    Result:=inherited GetValueAsInt64(AnInstance);
  end;
end;

procedure TcsRttiStreamDescription.SetValueAsBoolean(AnInstance: TObject; Value: Boolean);
begin
  SetValueAsInt64(AnInstance, Ord(Value));
end;

procedure TcsRttiStreamDescription.SetValueAsObject(AnInstance: TObject; Value: TObject);
begin
  if FPropInfo^.PropType^.Kind = tkClass then
    SetObjectProp(AnInstance, FPropInfo, Value)
  else
    inherited SetValueAsObject(AnInstance, Value);
end;

function TcsRttiStreamDescription.GetValueAsBoolean(AnInstance: TObject): Boolean;
begin
  Result := Boolean(GetValueAsInt64(AnInstance));
end;

function TcsRttiStreamDescription.CreateNewInstance(AParentInstance: TObject): TObject;
begin
  Result := nil;

  if Assigned(OnCreateNewInstance) and OnCreateNewInstance(Self, AParentInstance, Result) then
    Exit;

  if InstanceType = TcsitObject then
    begin
    Logger.Info('RTTI Create new ' + ClassDefinition.ClassName + ' instance.');
    Result := ClassDefinition.Create;
    end
  else
    raise EcsStreamException.CreateFmt('It is only possible for object-items to create new instances. [%s]', [PropertyName]);
end;

{ TcsRttiDescriber }

function TcsRttiDescriber.ObtainDescriptionForClass(AClass: TClass; AParent: TcsStreamDescription): TcsStreamDescription;
var
  Description: TcsStreamDescription;
  i, PropCount: Integer;
  PropList: PPropList;
  Prop: TcsStreamDescription;
begin
  if Assigned(AParent) then
    Logger.Info('RTTI Describe class ' + AClass.ClassName + '-'+AParent.PropertyName)
  else
    Logger.Info('RTTI Describe class ' + AClass.ClassName);
  Description := TcsRttiStreamDescription.Create(AParent);
  try
    Description.InstanceType := TcsitObject;
    Description.ClassDefinition := AClass;
    if DefaultImportNameStyle <> tcsinsNotSet then
      Description.ImportNameStyle := DefaultImportNameStyle;
    if DefaultExportNameStyle <> tcsensNotSet then
      Description.ExportNameStyle := DefaultExportNameStyle;

    PropCount := GetPropList(AClass, PropList);
    try
      for i := 0 to PropCount -1 do
        begin
        Prop := ObtainDescriptionForPropertyOfClass(Description, Self, AClass, PropList^[i]);
        Prop.PropertyName := PropList^[i]^.Name;
        Description.Properties.Add(Prop);
        end;
    finally
      FreeMem(PropList);
    end;

    OverrideDescriptionForKnownClasses(AClass, Description);

    Result := Description;
    Description := nil;
  finally
    Description.Free;
  end;
end;

function TcsRttiDescriber.ObtainDescriptionForPropertyOfClass(AParent: TcsStreamDescription;
  AClassDescription: TcsDescriber; AClass: TClass; AProperty: PPropInfo): TcsStreamDescription;
var
  Description: TcsRttiStreamDescription;
begin
  Logger.Info('RTTI Describe property ' + AClass.ClassName + ' - ' + AProperty^.Name);
  Description := ObtainDescriptionForType(AParent, AProperty^.PropType) as TcsRttiStreamDescription;
  try
    if Assigned(Description) then
      begin
      Description.PropInfo:=AProperty;
      if Assigned(Description.ListDescription) and (Description.ListDescription is TcsRttiStreamDescription) then
        TcsRttiStreamDescription(Description.ListDescription).PropInfo:=AProperty;
      end;
    Description.IsReadable := IsReadableProp(AProperty);
    Description.IsWriteable := (AProperty^.PropType^.Kind=tkClass) or IsWriteableProp(AProperty);

    if (tcsdfCreateClassInstances in Flags) and (AProperty^.PropType^.Kind=tkClass) and IsWriteableProp(AProperty) then
      Description.CanCreateClassInstances := True;

    Result := Description;
    Description := nil;
  finally
    Description.Free;
  end;
end;

function TcsRttiDescriber.ObtainDescriptionForType(AParent: TcsStreamDescription; APropType: PTypeInfo): TcsStreamDescription;
var
  Description: TcsRttiStreamDescription;
  TypeData: PTypeData;
begin
  if APropType^.Kind = tkClass then
    begin
    TypeData := GetTypeData(APropType);
    Result := ObtainDescriptionForClass(TypeData^.ClassType, AParent);

    if (tcsdfDynamicClassDescriptions in Flags) then
      Result.UseOnGetSubjectDescription := True;
    end
  else
    begin
    Description := TcsRttiStreamDescription.Create(AParent);
    try
      case APropType^.Kind of
        tkInteger: Description.InstanceType := TcsitInteger;
        tkInt64: Description.InstanceType := TcsitInt64;
        tkSString: Description.InstanceType := TcsitText;
        tkAString: Description.InstanceType := TcsitText;
        tkArray: Description.InstanceType := TcsitList;
        tkEnumeration: Description.InstanceType := TcsitEnumeration;
        tkBool: Description.InstanceType := TcsitBoolean;
        tkDynArray:
          begin
          Description.InstanceType := TcsitList;
          TypeData := GetTypeData(APropType);
          Description.ListDescription := ObtainDescriptionForType(AParent, TypeData^.ElType2);
          end;
        tkFloat:
          begin
          if APropType^.Name = 'TDateTime' then
            Description.InstanceType := tcsitDatetime
          else if APropType^.Name = 'TDate' then
            Description.InstanceType := tcsitDate
          else
            Description.InstanceType := tcsitFloat;
          end;
      end;
      Description.TypeInfo := APropType;
      Result := Description;
      Description := nil;
    finally
      Description.Free;
    end;
  end;
end;

end.

