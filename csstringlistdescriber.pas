unit csStringListDescriber;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  csModel;

type

  { TcsStringListDescriber }

  TcsStringListDescriber = class(TcsClassDescriber)
  protected
    function GetStringListValue(AnInstance: TObject; ADescription: TcsStreamDescription; out AValue: string): boolean;
    function SetStringListValue(AnInstance: TObject; ADescription: TcsStreamDescription; const AValue: string): boolean;

    function GetStringListItemCount(AnInstance: TObject; ADescription: TcsStreamDescription): Integer;
    function SetStringListItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
  public
    class function IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean; override;
    procedure AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject); override;
  end;


implementation

{ TcsStringListDescriber }

function TcsStringListDescriber.GetStringListValue(AnInstance: TObject; ADescription: TcsStreamDescription; out AValue: string): boolean;
begin
  if AnInstance is TStrings then
    begin
    AValue := TStrings(AnInstance).Strings[ADescription.Index];
    Result := True;
    end
  else
    begin
    AValue := '';
    Result := False;
    end;
end;

function TcsStringListDescriber.SetStringListValue(AnInstance: TObject; ADescription: TcsStreamDescription; const AValue: string): boolean;
begin
  if AnInstance is TStrings then
    begin
    TStrings(AnInstance).Strings[ADescription.Index] := AValue;
    Result := True;
    end
  else
    Result := False;
end;

function TcsStringListDescriber.GetStringListItemCount(AnInstance: TObject; ADescription: TcsStreamDescription): Integer;
begin
  if AnInstance is TStrings then
    Result := TStrings(AnInstance).Count
  else
    Result := -1;
end;

function TcsStringListDescriber.SetStringListItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
var
  i: Integer;
begin
  if AnInstance is TStrings then
    begin
    for i := 0 to AValue -1 do
      TStrings(AnInstance).Add('');
    Result := True;
    end
  else
    Result := False;
end;

class function TcsStringListDescriber.IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean;
begin
  Result := AClass.InheritsFrom(TStrings);
end;

procedure TcsStringListDescriber.AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject);
var
  Description: TcsStreamDescription;
begin
  ADescription.InstanceType := TcsitList;
  ADescription.OnGetItemCount := @GetStringListItemCount;
  ADescription.OnSetItemCount := @SetStringListItemCount;

  Description := TcsStreamDescription.Create(ADescription);
  try
    Description.InstanceType := TcsitText;
    Description.OnGetValueAsString := @GetStringListValue;
    Description.OnSetValueAsString := @SetStringListValue;

    ADescription.ListDescription := Description;
    Description := nil;
  finally
    Description.Free;
  end;
end;

end.

