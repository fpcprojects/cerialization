unit csStreamDescriptionStore;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  csModel;

type

  { TcsStreamDescriptionStore }

  TcsStreamDescriptionStore = class
  protected
    type
      TcsStreamDescriptionKey = record
        ObjClass: TClass;
        Tag: string;
      end;
      TcsStreamDescriptionStoreMap = specialize TAVLTreeMap<TcsStreamDescriptionKey, TcsStreamDescription>;
  private
    FDescriber: TcsDescriber;
    FDescriptionMap: TcsStreamDescriptionStoreMap;
  protected
    function GetSubjectDescription(AnDescription: TcsStreamDescription; AnInstance: TObject): TcsStreamDescription; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure StoreDescription(AClass: TClass; ADescription: TcsStreamDescription; ATag: string = ''); virtual;
    function GetDescription(AClass: TClass; ATag: string = ''): TcsStreamDescription; virtual;
    function CloneDescription(AClass: TClass; ASourceTag, ATargetTag: string): TcsStreamDescription; virtual;
    function FindDescription(AClass: TClass; ATag: string = ''): TcsStreamDescription; virtual;
    property Describer: TcsDescriber read FDescriber write FDescriber;
  end;

implementation

{ TcsStreamDescriptionStore }

constructor TcsStreamDescriptionStore.Create;
begin
  inherited Create;
  FDescriptionMap := TcsStreamDescriptionStoreMap.Create;
end;

destructor TcsStreamDescriptionStore.Destroy;
var
  Pair: specialize TPair<TcsStreamDescriptionKey, TcsStreamDescription>;
begin
  for Pair in FDescriptionMap do
    Pair.Value.Free;
  FDescriptionMap.Clear();
  FDescriptionMap.Free;
  inherited Destroy;
end;

function TcsStreamDescriptionStore.GetDescription(AClass: TClass; ATag: string): TcsStreamDescription;
var
  Key: TcsStreamDescriptionKey;
begin
  Key.ObjClass := AClass;
  Key.Tag := ATag;
  if FDescriptionMap.ContainsKey(Key) then
    Result := FDescriptionMap.Items[Key]
  else
    Result := nil;

  // ToDo: decide when to create the description, based on an enumeration.
  if not Assigned(Result) and Assigned(FDescriber) then
    begin
    Result := FDescriber.ObtainDescriptionForClass(AClass, nil);

    if (tcsdfDynamicClassDescriptions in FDescriber.Flags) and not Assigned(Result.GetEffectiveOnGetSubjectDescription) then
      Result.OnGetSubjectDescription := @GetSubjectDescription;

    if Assigned(Result) then
      StoreDescription(AClass, Result, ATag)
    else
      raise Exception.CreateFmt('There is no description for class [%s] with tag [%s]', [AClass.ClassName, ATag] );
    end;
end;

procedure TcsStreamDescriptionStore.StoreDescription(AClass: TClass; ADescription: TcsStreamDescription; ATag: string);
var
  Key: TcsStreamDescriptionKey;
begin
  Key.ObjClass := AClass;
  Key.Tag := ATag;
  FDescriptionMap.Add(Key, ADescription);
end;

function TcsStreamDescriptionStore.FindDescription(AClass: TClass; ATag: string): TcsStreamDescription;
var
  Key: TcsStreamDescriptionKey;
begin
  Key.ObjClass := AClass;
  Key.Tag := ATag;
  Result := FDescriptionMap.Items[Key];
end;

function TcsStreamDescriptionStore.CloneDescription(AClass: TClass; ASourceTag, ATargetTag: string): TcsStreamDescription;
var
  SourceDescription: TcsStreamDescription;
begin
  SourceDescription := FindDescription(AClass, ASourceTag);

  if not Assigned(SourceDescription) then
    raise EcsStreamException.CreateFmt('Description with tag [%s] for class [%s] does not exist', [ASourceTag, AClass.ClassName]);

  Result := SourceDescription.Clone;
  StoreDescription(AClass, Result, ATargetTag);
end;

function TcsStreamDescriptionStore.GetSubjectDescription(AnDescription: TcsStreamDescription; AnInstance: TObject): TcsStreamDescription;
begin
  if Assigned(AnInstance) then
    Result := GetDescription(AnInstance.ClassType)
  else
    Result := AnDescription.DefaultSubObjectDescription;
end;

end.

