unit csJSONStream;

{$mode objfpc}{$H+}

interface

uses
  sysutils,
  fpjson,
  csModel,
  DateUtils,
  csStream;

type

  { TcsJSONStream }

  TcsJSONStream = class(TcsModelStream)
  protected
    function ElementToJSON(AnInstance: TObject; Description: TcsStreamDescription; Kind: TcsDescriptionKind): TJSONData;
    procedure FillElementWithJSONData(AnInstance: TObject; AJSONData: TJSONData; Description: TcsStreamDescription; Kind: TcsDescriptionKind);
  public
    function ObjectToModel(AnInstance: TObject; AStreamDescription: TcsStreamDescription): TObject; override;
    procedure ModelToObject(AModel: TObject; AnInstance: TObject; AStreamDescription: TcsStreamDescription); override;
    procedure AddProperty(AnInstance: TObject; AModel: TObject; AProperty: TcsStreamDescription); virtual;
    procedure GetProperty(AnInstance: TObject; AModel: TObject; AProperty: TcsStreamDescription); override;
  end;

implementation

{ TcsJSONStream }

function TcsJSONStream.ObjectToModel(AnInstance: TObject; AStreamDescription: TcsStreamDescription): TObject;
begin
  if FilterElement(AnInstance, AStreamDescription) then
    Result := ElementToJSON(AnInstance, AStreamDescription, TcsdkObject)
  else
    Result := nil;
end;

procedure TcsJSONStream.AddProperty(AnInstance: TObject; AModel: TObject; AProperty: TcsStreamDescription);
var
  JS: TJSONObject;
begin
  if AProperty.IsReadable then
    begin
    JS := AModel as TJSONObject;
    if FilterElement(AnInstance, AProperty) then
      JS.Add(FormatName(AProperty.PropertyName, AProperty.GetEffectiveExportNameStyle), ElementToJSON(AnInstance, AProperty, TcsdkProperty));
    end;
end;

procedure TcsJSONStream.GetProperty(AnInstance: TObject; AModel: TObject; AProperty: TcsStreamDescription);
var
  JS: TJSONObject;
  JSPropData: TJSONData;
  i: Integer;
begin
  if AProperty.IsWriteable then
    begin
    JS := AModel as TJSONObject;
    if AProperty.GetEffectiveImportNameStyle = tcsinsCaseInsensitive then
      begin
      i := JS.IndexOfName(AProperty.PropertyName, True);
      if i > -1 then
        JSPropData:=JS.Items[i]
      else
        JSPropData := nil;
      end
    else
      JSPropData := JS.Find(FormatName(AProperty.PropertyName, AProperty.GetEffectiveImportNameStyle));
    FillElementWithJSONData(AnInstance, JSPropData, AProperty, TcsdkProperty);
    end;
end;

function TcsJSONStream.ElementToJSON(AnInstance: TObject; Description: TcsStreamDescription; Kind: TcsDescriptionKind): TJSONData;
var
  JSArray: TJSONArray;
  I: Integer;
  SubObjectDescription, ItemDescription: TcsStreamDescription;
  ObjInstance: TObject;
  DT: Extended;
begin
  Result := nil;
  case Description.InstanceType of
    TcsitText,
    TcsitEnumeration:
      Result := TJSONString.Create(Description.GetValueAsString(AnInstance));
    TcsitInteger:
      Result := TJSONIntegerNumber.Create(Description.GetValueAsInteger(AnInstance));
    tcsitFloat:
      Result := TJSONFloatNumber.Create(Description.GetValueAsFloat(AnInstance));
    TcsitBoolean:
      Result := TJSONBoolean.Create(Description.GetValueAsBoolean(AnInstance));
    tcsitDate:
      begin
      DT := Description.GetValueAsFloat(AnInstance);
      if DT=0 then
        Result := TJSONNull.Create()
      else
        Result := TJSONString.Create(Description.FormatDateAsString(DT));
      end;
    tcsitDatetime:
      begin
      DT := Description.GetValueAsFloat(AnInstance);
      if DT=0 then
        Result := TJSONNull.Create()
      else
        Result := TJSONString.Create(Description.FormatDateTimeAsString(DT));
      end;
    TcsitInt64:
      Result := TJSONInt64Number.Create(Description.GetValueAsInt64(AnInstance));
    TcsitList:
      begin
      if Kind = TcsdkProperty then
        ObjInstance := Description.GetListObject(AnInstance)
      else
        ObjInstance := AnInstance;
      JSArray := TJSONArray.Create();
      try
        for I := 0 to Description.GetItemCount(ObjInstance) -1 do
          begin
          ItemDescription := Description.GetItem(ObjInstance, I);
          if not Assigned(ItemDescription) then
            raise EcsStreamException.CreateFmt('Item-discription for array-element [%s] with index [%d] not found', [Description.PropertyName, i]);

          PrepareItemDescriptionForIndex(ItemDescription, Description, ObjInstance, I);

          if FilterElement(ObjInstance, ItemDescription) then
            JSArray.Add(ElementToJSON(ObjInstance, ItemDescription, TcsdkProperty));
          end;
        Result := JSArray;
        JSArray := nil;
      finally
        JSArray.Free;
      end;
      end;
    TcsitObject:
      begin
      if Kind = TcsdkProperty then
        ObjInstance := Description.GetValueAsObject(AnInstance)
      else
        ObjInstance := AnInstance;
      if Assigned(ObjInstance) then
        begin
        Result := TJSONObject.Create;

        SubObjectDescription := Description.GetSubObjectDescription(ObjInstance);
        if not Assigned(SubObjectDescription) then
          SubObjectDescription := Description;

        for i := 0 to SubObjectDescription.Properties.Count -1 do
          begin
          AddProperty(ObjInstance, Result, SubObjectDescription.Properties[i]);
          end;
        end
      else
        Result := TJSONNull.Create;
      end;
  end;
end;

procedure TcsJSONStream.FillElementWithJSONData(AnInstance: TObject; AJSONData: TJSONData;
  Description: TcsStreamDescription; Kind: TcsDescriptionKind);
var
  JSArray: TJSONArray;
  I: Integer;
  ItemDescription, SubObjectDescription: TcsStreamDescription;
  ObjInstance: TObject;
begin
  if Assigned(AJSONData) then
    begin
    Logger.Info('Json -> ' + GetLogDescription(AnInstance, Description) + ' : ' + AJSONData.AsJSON);
    if (AJSONData.JSONType<>jtNull) then
      begin
      case Description.InstanceType of
        TcsitText:
          Description.SetValueAsString(AnInstance, AJSONData.AsString);
        TcsitInteger:
          Description.SetValueAsInteger(AnInstance, AJSONData.AsInteger);
        TcsitInt64:
          Description.SetValueAsInt64(AnInstance, AJSONData.AsInt64);
        TcsitEnumeration:
          Description.SetValueAsString(AnInstance, AJSONData.AsString);
        tcsitFloat:
          Description.SetValueAsFloat(AnInstance, AJSONData.AsFloat);
        tcsitDate:
          Description.SetValueAsFloat(AnInstance, Description.DateStringToDate(AJSONData.AsString));
        TcsitBoolean:
          Description.SetValueAsBoolean(AnInstance, AJSONData.AsBoolean);
        tcsitDatetime:
          Description.SetValueAsFloat(AnInstance, Description.DateTimeStringToDate(AJSONData.AsString));
        TcsitList:
          begin
          if Kind = TcsdkProperty then
            ObjInstance := Description.GetListObject(AnInstance)
          else
            ObjInstance := AnInstance;

          if AJSONData.JSONType<>jtArray then
            raise EcsStreamException.CreateFmt('Json-array expected but got ''%s''', [AJSONData.AsJSON]);

          JSArray := AJSONData as TJSONArray;
          Description.SetItemCount(ObjInstance, JSArray.Count);

          for I := 0 to JSArray.Count -1 do
            begin
            ItemDescription := Description.GetItem(AnInstance, I);
            PrepareItemDescriptionForIndex(ItemDescription, Description, ObjInstance, I);
            FillElementWithJSONData(ObjInstance, JSArray.Items[I], ItemDescription, TcsdkProperty);
            end;
          end;
        TcsitObject:
          begin
          if Kind = TcsdkProperty then
            ObjInstance := Description.GetSetValueAsObject(AnInstance)
          else
            ObjInstance := AnInstance;

          if Assigned(ObjInstance) then
            begin
            SubObjectDescription := Description.GetSubObjectDescription(ObjInstance);
            if not Assigned(SubObjectDescription) then
              SubObjectDescription := Description;

            for i := 0 to SubObjectDescription.Properties.Count -1 do
              begin
              try
                GetProperty(ObjInstance, AJSONData, SubObjectDescription.Properties[i]);
              except
                on E: Exception do
                  raise EcsStreamException.CreateFmt('Exception while processing property [%s]: %s', [SubObjectDescription.Properties[i].PropertyName, E.Message]);
              end;
              end;
            end;
          end;
      end;
      end;
    // else .. based on some flag clear the value in the object?!?!
    end
  else
    begin
    Logger.Info('Json -> ' + GetLogDescription(AnInstance, Description) + ' - no matching element found');
    end;
end;

procedure TcsJSONStream.ModelToObject(AModel: TObject; AnInstance: TObject; AStreamDescription: TcsStreamDescription);
begin
  FillElementWithJSONData(AnInstance, AModel as TJSONData, AStreamDescription, tcsdkObject);
end;

end.

