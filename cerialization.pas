{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit cerialization;

{$warn 5023 off : no warning about unused units}
interface

uses
  csModel, csStream, csJSONStream, csRttiModel, csStreamHelper, csJSONRttiStreamHelper, 
  csStreamDescriptionStore, csCollectionDescriber, csRegisterCommonClassDescribers, 
  csGenericCollectionsDescriber;

implementation

end.
