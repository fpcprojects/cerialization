unit csGenericCollectionsDescriber;

{$mode objfpc}{$H+}

interface

uses
  Generics.Collections,
  TypInfo,
  sysutils,
  csModel;

type

  { TcsGenericCollectionTListDescriber }

  generic TcsGenericCollectionTListDescriber<T> = class(TcsClassDescriber)
  protected
    function GetItemCount(AnInstance: TObject; ADescription: TcsStreamDescription): Integer;
    function GetListObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
    function SetItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
  public
    class function IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean; override;
    procedure AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject); override;
  end;

  { TcsGenericCollectionTListDescriber }

  generic TcsGenericCollectionTListObjectDescriber<T: TObject> = class(specialize TcsGenericCollectionTListDescriber<T>)
  protected
    function GetObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
    function SetItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
  public
    procedure AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject); override;
  end;


implementation

{ TcsGenericCollectionTListDescriber }

class function TcsGenericCollectionTListDescriber.IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean;
begin
  Result := AClass.InheritsFrom(specialize TList<T>);
end;

procedure TcsGenericCollectionTListDescriber.AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject);
var
  Description: TcsStreamDescription;
begin
  ADescription.InstanceType := TcsitList;
  ADescription.OnGetListObject := @GetListObject;
  ADescription.OnGetItemCount := @GetItemCount;
  ADescription.OnSetItemCount := @SetItemCount;

  Description := TcsStreamDescription.Create(ADescription);
  try
    Description.InstanceType := TcsitObject;
    //Description.OnGetValueAsObject := @GetObject;
    if (tcsdfDynamicClassDescriptions in ADescriber.Flags) then
      Description.UseOnGetSubjectDescription := True;
    Description.CanCreateClassInstances := (tcsdfCreateClassInstances in ADescriber.Flags);
    //Description.DefaultSubObjectDescription := ADescriber.ObtainDescriptionForClass(T, ADescription);
    ADescription.ListDescription := Description;
    Description := nil;
  finally
    Description.Free;
  end;
end;

function TcsGenericCollectionTListDescriber.GetItemCount(AnInstance: TObject; ADescription: TcsStreamDescription): Integer;
begin
  if AnInstance is specialize TList<T> then
    Result := specialize TList<T>(AnInstance).Count
  else
    Result := -1;
end;

function TcsGenericCollectionTListDescriber.GetListObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
begin
  Result := ADescription.GetValueAsObject(AnInstance);
end;

function TcsGenericCollectionTListDescriber.SetItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
var
  ListInstance: specialize TList<T>;
begin
  if AnInstance is specialize TList<T> then
    begin
    ListInstance := specialize TList<T>(AnInstance);
    ListInstance.Count := AValue;
    end
  else
    Result := False;
end;

function TcsGenericCollectionTListObjectDescriber.SetItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
var
  ListInstance: specialize TList<T>;
  i: Integer;
  SubObjectDescription: TcsStreamDescription;
begin
  if AnInstance is specialize TList<T> then
    begin
    ListInstance := specialize TList<T>(AnInstance);
    ListInstance.Count := AValue;
    if ADescription.ListDescription.CanCreateClassInstances then
      for i := 0 to AValue -1 do
        begin
        if not Assigned(ListInstance.Items[i]) then
          begin
          SubObjectDescription := ADescription.ListDescription.GetSubObjectDescription(nil);
          if Assigned(SubObjectDescription) then
            ListInstance.Items[i] := SubObjectDescription.CreateNewInstance(AnInstance) as T;
          end;
        end;
    end
  else
    Result := False;
end;

function TcsGenericCollectionTListObjectDescriber.GetObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
begin
  if AnInstance is specialize TList<T> then
    Result := specialize TList<T>(AnInstance).Items[ADescription.Index]
  else
    Result := nil;
end;

{ TcsGenericCollectionTListObjectDescriber }

procedure TcsGenericCollectionTListObjectDescriber.AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject);
var
  Description: TcsStreamDescription;
begin
  Logger.Info('CollectionTList Adapt description for class ' + AClass.ClassName);

  ADescription.InstanceType := TcsitList;
  ADescription.OnGetListObject := @GetListObject;
  ADescription.OnGetItemCount := @GetItemCount;
  ADescription.OnSetItemCount := @SetItemCount;

  Description := TcsStreamDescription.Create(ADescription);
  try
    Description.InstanceType := TcsitObject;
    Description.OnGetValueAsObject := @GetObject;
    if (tcsdfDynamicClassDescriptions in ADescriber.Flags) then
      Description.UseOnGetSubjectDescription := True;
    Description.CanCreateClassInstances := (tcsdfCreateClassInstances in ADescriber.Flags);
    Description.DefaultSubObjectDescription := ADescriber.ObtainDescriptionForClass(T, ADescription);
    ADescription.ListDescription := Description;
    Description := nil;
  finally
    Description.Free;
  end;
end;

end.

