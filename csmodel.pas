unit csModel;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  DateUtils,
  Generics.Collections,
  TLoggerUnit;

type
  EcsStreamException = class(Exception);
  TcsInstanceType = (TcsitUnknown, TcsitObject, TcsitInteger, TcsitInt64, tcsitFloat, tcsitDate, tcsitDatetime, TcsitText, TcsitBoolean, TcsitEnumeration, TcsitList);
  TcsStreamDescription = class;
  TcsExportNameStyle = (tcsensNotSet, tcsensLiteral, tcsensLowerCase, tcsensUpperCase, tcsensLowerCaseFirstChar);
  TcsImportNameStyle = (tcsinsNotSet, tcsinsCaseSensitive, tcsinsCaseInsensitive, tcsinsLowerCase, tcsinsUpperCase, tcsinsLowerCaseFirstChar);
  TcsDescriptionKind = (tcsdkProperty, tcsdkObject);
  TcsDescriberFlag = (
    // Sometimes the description of a subject is not known at the moment the
    // description is made. (For example when the RTTI does not know that kind
    // of objects there are in a collection)
    // In some of those cases the description can be retrieved while streaming
    // the subject. Use this setting to enable this behavior.
    tcsdfDynamicClassDescriptions,
    tcsdfCreateClassInstances);
  TcsDescriberFlagSet = set of TcsDescriberFlag;

  TcsGetItemCount = function(AnInstance: TObject; ADescription: TcsStreamDescription): Integer of object;
  TcsSetItemCount = function(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean of object;
  TcsGetValueAsObject = function(AnInstance: TObject; ADescription: TcsStreamDescription): TObject of object;
  TcsGetObjectDescription = function(AnDescription: TcsStreamDescription; AnInstance: TObject): TcsStreamDescription of object;

  TcsGetValueAsString = function(AnInstance: TObject; ADescription: TcsStreamDescription; out AValue: string): boolean of object;
  TcsSetValueAsString = function(AnInstance: TObject; ADescription: TcsStreamDescription; const AValue: string): boolean of object;
  TcsOnFilter = function(AnInstance: TObject; ADescription: TcsStreamDescription): boolean of object;
  TcsCreateNewInstance = function(ADescription: TcsStreamDescription; AParentInstance: TObject; out AnInstance: TObject): Boolean of object;

  TcsGenStreamDescriptionList = specialize TObjectList<TcsStreamDescription>;

  { TcsLogBaseClass }

  TcsLogBaseClass = class
  private
    FLogger: TLogger;
  protected
    function GetLogDescription(const AnInstance: TObject; const Description: TcsStreamDescription): string;
    function GetLogger: TLogger;
    property Logger: TLogger read GetLogger;
  end;

  { TcsStreamDescriptionList }

  TcsStreamDescriptionList = class(TcsGenStreamDescriptionList)
  public
    function FindByPropertyName(const AName: string): TcsStreamDescription;
    procedure Assign(ASource: TcsStreamDescriptionList); virtual;
  end;

  { TcsStreamDescription }

  TcsStreamDescription = class(TcsLogBaseClass)
  private
    FInstanceType: TcsInstanceType;
    FProperties: TcsStreamDescriptionList;
    FExportNameStyle: TcsExportNameStyle;
    FImportNameStyle: TcsImportNameStyle;
    FParent: TcsStreamDescription;
    FPropertyName: string;
    FStaticItemCount: Int64;
    FOnGetItemCount: TcsGetItemCount;
    FListDescription: TcsStreamDescription;
    FOnGetValueAsObject: TcsGetValueAsObject;
    FIndex: Integer;
    FOnGetListObject: TcsGetValueAsObject;
    FDefaultSubObjectDescription: TcsStreamDescription;
    FOnGetSubjectDescription: TcsGetObjectDescription;
    FOnSetItemCount: TcsSetItemCount;
    FIsReadable: Boolean;
    FIsWriteable: Boolean;
    FOnGetValueAsString: TcsGetValueAsString;
    FOnSetValueAsString: TcsSetValueAsString;
    FOnFilter: TcsOnFilter;
    FOnCreateNewInstance: TcsCreateNewInstance;
    FClassDefinition: TClass;
    FCanCreateClassInstances: Boolean;
    FUseOnGetSubjectDescription: Boolean;

    procedure SetListDescription(AValue: TcsStreamDescription);
    procedure SetDefaultSubObjectDescription(AValue: TcsStreamDescription);
  protected
    function GetPropertyName: string; virtual;
    procedure SetPropertyName(AValue: string); virtual;
    procedure Assign(ASource: TcsStreamDescription); virtual;
  public
    constructor Create(AParent: TcsStreamDescription); virtual;
    destructor Destroy; override;

    function GetValueAsString(AnInstance: TObject): string; virtual;
    function GetValueAsInteger(AnInstance: TObject): Integer; virtual;
    function GetValueAsInt64(AnInstance: TObject): Int64; virtual;
    function GetValueAsFloat(AnInstance: TObject): Extended; virtual;
    function GetValueAsObject(AnInstance: TObject): TObject; virtual;
    function GetValueAsBoolean(AnInstance: TObject): Boolean; virtual;

    function FormatDateAsString(ADate: TDate): string; virtual;
    function FormatDateTimeAsString(ADateTime: TDateTime): string; virtual;
    function DateStringToDate(ADate: string): TDate; virtual;
    function DateTimeStringToDate(ADateTime: string): TDateTime; virtual;

    function GetListObject(AnInstance: TObject): TObject; virtual;
    function GetSubObjectDescription(AnInstance: TObject): TcsStreamDescription; virtual;

    procedure SetValueAsString(AnInstance: TObject; Value: string); virtual;
    procedure SetValueAsInteger(AnInstance: TObject; Value: Integer); virtual;
    procedure SetValueAsInt64(AnInstance: TObject; Value: Int64); virtual;
    procedure SetValueAsFloat(AnInstance: TObject; Value: Extended); virtual;
    procedure SetValueAsBoolean(AnInstance: TObject; Value: Boolean); virtual;
    procedure SetValueAsObject(AnInstance: TObject; Value: TObject); virtual;
    // Use this to set an object-value. Setting an object-value is different,
    // though. In some cases an existing instance of the object is used, in
    // other cases a new instance is created, depending on the settings.
    // GetSetValueAsObject returns the instance that has to be used to set
    // a value. (Or nil if no instance is available)
    function GetSetValueAsObject(AnInstance: TObject): TObject; virtual;

    function CreateNewInstance(AParentInstance: TObject): TObject; virtual;

    function Clone: TcsStreamDescription; virtual;

    function GetItemCount(AnInstance: TObject): Int64; virtual;
    function GetItem(AnInstance: TObject; const Index: Integer): TcsStreamDescription; virtual;
    function SetItemCount(AnInstance: TObject; AValue: Integer): Boolean; virtual;

    function GetEffectiveExportNameStyle: TcsExportNameStyle;
    function GetEffectiveImportNameStyle: TcsImportNameStyle;
    function GetEffectiveOnGetSubjectDescription: TcsGetObjectDescription;

    property Parent: TcsStreamDescription read FParent;
    property InstanceType: TcsInstanceType read FInstanceType write FInstanceType;
    property Properties: TcsStreamDescriptionList read FProperties;
    property ExportNameStyle: TcsExportNameStyle read FExportNameStyle write FExportNameStyle;
    property ImportNameStyle: TcsImportNameStyle read FImportNameStyle write FImportNameStyle;

    // When this property is > -1, it is used as the number of items in a TcsitList.
    property StaticItemCount: Int64 read FStaticItemCount write FStaticItemCount;
    // When there is no StaticItemCount, this callback is called to retrieve the
    // amount of items in a TcsitList.
    property OnGetItemCount: TcsGetItemCount read FOnGetItemCount write FOnGetItemCount;
    property OnSetItemCount: TcsSetItemCount read FOnSetItemCount write FOnSetItemCount;

    property OnGetValueAsObject: TcsGetValueAsObject read FOnGetValueAsObject write FOnGetValueAsObject;
    property OnGetListObject: TcsGetValueAsObject read FOnGetListObject write FOnGetListObject;

    property OnGetValueAsString: TcsGetValueAsString read FOnGetValueAsString write FOnGetValueAsString;
    property OnSetValueAsString: TcsSetValueAsString read FOnSetValueAsString write FOnSetValueAsString;

    property OnCreateNewInstance: TcsCreateNewInstance read FOnCreateNewInstance write FOnCreateNewInstance;
    property ClassDefinition: TClass read FClassDefinition write FClassDefinition;
    // When set to true, a description can try to create a new instance of the class
    // during de-serialization. Use with care. Do not forget that the instances
    // have to be released too.
    property CanCreateClassInstances: Boolean read FCanCreateClassInstances write FCanCreateClassInstances;
    // There is a separate UseOnGetSubjectDescription property, because a describer may
    // decide that OnGetSubjectDescription could be used, but it might not be able
    // to set OnGetSubjectDescription itself.
    property UseOnGetSubjectDescription: Boolean read FUseOnGetSubjectDescription write FUseOnGetSubjectDescription;

    // When this event returns false, the element is omitted.
    property OnFilter: TcsOnFilter read FOnFilter write FOnFilter;

    property PropertyName: string read GetPropertyName write SetPropertyName;
    property Index: Integer read FIndex write FIndex;

    // By setting the ListDescription or DefaultSubObjectDescription this Descripion
    // becomes the owner of the ListDescription/DefaultSubObjectDescription. And will
    // thus free them once they are not needed anymore.
    // The idea behind this is that each description (and it's parts) stands on it's
    // own, and does not depend on other descriptions.
    property ListDescription: TcsStreamDescription read FListDescription write SetListDescription;
    property DefaultSubObjectDescription: TcsStreamDescription read FDefaultSubObjectDescription write SetDefaultSubObjectDescription;

    property OnGetSubjectDescription: TcsGetObjectDescription read FOnGetSubjectDescription write FOnGetSubjectDescription;
    property IsReadable: Boolean read FIsReadable write FIsReadable;
    property IsWriteable: Boolean read FIsWriteable write FIsWriteable;
  end;
  TcsStreamDescriptionClass = class of TcsStreamDescription;

  { TcsDescriber }

  TcsClassDescriberFactory = class;
  TcsDescriber = class(TcsLogBaseClass)
  private
    class var FGlobalDefaultExportNameStyle: TcsExportNameStyle;
    class var FGlobalDefaultImportNameStyle: TcsImportNameStyle;
  private
    FFlags: TcsDescriberFlagSet;
    FDefaultExportNameStyle: TcsExportNameStyle;
    FDefaultImportNameStyle: TcsImportNameStyle;
    FClassDescriberFactory: TcsClassDescriberFactory;
    FIsClassDescriberFactoryOwner: Boolean;
  public
    // When these class-properties are set, they are the default for all newly created
    // TcsDescriber instances.
    class property GlobalDefaultExportNameStyle: TcsExportNameStyle read FGlobalDefaultExportNameStyle write FGlobalDefaultExportNameStyle;
    class property GloblaDefaultImportNameStyle: TcsImportNameStyle read FGlobalDefaultImportNameStyle write FGlobalDefaultImportNameStyle;
  public
    constructor Create(); virtual;
    destructor Destroy; override;
    function ObtainDescriptionForClass(AClass: TClass; AParent: TcsStreamDescription): TcsStreamDescription; virtual; abstract;
    procedure SetClassDescriberFactory(AClassDescriberFactory: TcsClassDescriberFactory; ABecomeOwner: Boolean);
    procedure CloneGlobalClassDescriberFactory();

    procedure OverrideDescriptionForKnownClasses(AClass: TClass; ADescription: TcsStreamDescription); virtual;

    property Flags: TcsDescriberFlagSet read FFlags write FFlags;
    property DefaultExportNameStyle: TcsExportNameStyle read FDefaultExportNameStyle write FDefaultExportNameStyle;
    property DefaultImportNameStyle: TcsImportNameStyle read FDefaultImportNameStyle write FDefaultImportNameStyle;

    property ClassDescriberFactory: TcsClassDescriberFactory read FClassDescriberFactory;
  end;

  { TcsClassDescriber }

  TcsClassDescriber = class(TcsLogBaseClass)
  public
    constructor Create; virtual;
    procedure Assign(const ASourceDescriber: TcsClassDescriber); virtual;
    function IsApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean; virtual;
    class function IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean; virtual;
    procedure AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject); virtual; abstract;
  end;
  TcsClassDescriberClass = class of TcsClassDescriber;

  { TcsClassDescriberFactory }

  TcsClassDescriberFactory = class
  private
    type
      TDescriptionItem = record
        Weight: integer;
        DescriberClass: TcsClassDescriberClass;
        Describer: TcsClassDescriber;
      end;
      TDescriptionItemList = specialize TList<TDescriptionItem>;
  private
    FDescribtionList: TDescriptionItemList;
    class var FInstance: TcsClassDescriberFactory;
  protected
    class function GetInstance: TcsClassDescriberFactory; static;
    class destructor Destroy;
    function GetClassDescriber(AnIndex: Integer): TcsClassDescriber; overload;
  public
    constructor Create;
    destructor Destroy; override;
    procedure RegisterClassDescriber(AClassDescriberClass: TcsClassDescriberClass; AWeight: Integer);
    function GetClassDescriberFor(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): TcsClassDescriber;
    function GetClassDescriber(AClassDescriberClass: TcsClassDescriberClass): TcsClassDescriber; overload;
    procedure Assign(const ASourceFactory: TcsClassDescriberFactory);
    procedure Clear;
    class property Instance: TcsClassDescriberFactory read GetInstance;
  end;

implementation

{ TcsLogBaseClass }

function TcsLogBaseClass.GetLogDescription(const AnInstance: TObject; const Description: TcsStreamDescription): string;
begin
  if Assigned(AnInstance) then
    begin
    if Description.PropertyName<>'' then
      Result := 'property ' + Description.PropertyName + ' of class ' + AnInstance.ClassName
    else
      Result := 'class ' + AnInstance.ClassName;
    end
  else
    Result := Description.PropertyName;
end;

function TcsLogBaseClass.GetLogger: TLogger;
begin
  if not Assigned(FLogger) then
    FLogger := TLogger.GetInstance('cerialization');
  Result := FLogger;
end;

{ TcsClassDescriberFactory }

procedure TcsClassDescriberFactory.Assign(const ASourceFactory: TcsClassDescriberFactory);
var
  SourceRec: TDescriptionItem;
  NewRec: TDescriptionItem;
  i: Integer;
begin
  Clear;
  for i := 0 to ASourceFactory.FDescribtionList.Count -1 do
    begin
    SourceRec := ASourceFactory.FDescribtionList.Items[i];
    NewRec := SourceRec;
    if Assigned(SourceRec.Describer) then
      begin
      NewRec.Describer := SourceRec.DescriberClass.Create;
      NewRec.Describer.Assign(SourceRec.Describer);
      end;
    FDescribtionList.Add(NewRec);
    end;
end;

procedure TcsClassDescriberFactory.Clear;
var
  i: Integer;
begin
  for i := 0 to FDescribtionList.Count -1 do
    begin
    if Assigned(FDescribtionList[i].Describer) then
      FDescribtionList[i].Describer.Free;
    end;
  FDescribtionList.Clear;
end;

function TcsClassDescriberFactory.GetClassDescriber(AClassDescriberClass: TcsClassDescriberClass): TcsClassDescriber;
var
  i: Integer;
  Rec: TDescriptionItem;
begin
  Result := nil;
  for i := 0 to FDescribtionList.Count -1 do
    begin
    if FDescribtionList[i].DescriberClass = AClassDescriberClass then
      begin
      if not Assigned(FDescribtionList[i].Describer) then
        begin
        Rec := FDescribtionList.Items[i];
        Rec.Describer := AClassDescriberClass.Create;
        FDescribtionList.Items[i] := rec;
        end;
      Result := FDescribtionList[i].Describer;
      Exit;
      end;
    end;
end;

function TcsClassDescriberFactory.GetClassDescriberFor(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): TcsClassDescriber;
var
  i: Integer;
  ClassDescr: TcsClassDescriber;
begin
  Result := nil;
  for i := 0 to FDescribtionList.Count -1 do
    begin
    if FDescribtionList.Items[i].DescriberClass.IsGenerallyApplicableTo(AClass, AnInstance, AnInstanceParent) then
      begin
      ClassDescr := GetClassDescriber(i);
      if ClassDescr.IsApplicableTo(AClass, AnInstance, AnInstanceParent) then
        begin
        Result := ClassDescr;
        Exit;
        end;
      end;
    end;
end;

class function TcsClassDescriberFactory.GetInstance: TcsClassDescriberFactory;
begin
  if not Assigned(FInstance) then
    FInstance := TcsClassDescriberFactory.Create;
  Result := FInstance;
end;

procedure TcsClassDescriberFactory.RegisterClassDescriber(AClassDescriberClass: TcsClassDescriberClass; AWeight: Integer);
var
  i: Integer;
  Rec: TDescriptionItem;
begin
  for i := 0 to FDescribtionList.Count -1 do
    if FDescribtionList[i].DescriberClass = AClassDescriberClass then
      raise EcsStreamException.CreateFmt('Class-describer class [%s] has already been registered.', [AClassDescriberClass.ClassName]);
  Rec := Default(TDescriptionItem);
  Rec.DescriberClass := AClassDescriberClass;
  Rec.Weight:= AWeight;
  i := 0;
  while (i < FDescribtionList.Count) and (FDescribtionList[i].Weight < AWeight) do
    inc(i);
  FDescribtionList.Insert(i, Rec);
end;

class destructor TcsClassDescriberFactory.Destroy;
begin
  FInstance.Free;
end;

constructor TcsClassDescriberFactory.Create;
begin
  FDescribtionList := TDescriptionItemList.Create;
end;

destructor TcsClassDescriberFactory.Destroy;
begin
  Clear;
  FDescribtionList.Destroy;
end;

function TcsClassDescriberFactory.GetClassDescriber(AnIndex: Integer): TcsClassDescriber;
var
  Rec: TDescriptionItem;
begin
  if not Assigned(FDescribtionList[AnIndex].Describer) then
    begin
    Rec := FDescribtionList.Items[AnIndex];
    Rec.Describer := FDescribtionList[AnIndex].DescriberClass.Create;
    FDescribtionList.Items[AnIndex] := rec;
    end;
  Result := FDescribtionList[AnIndex].Describer;
end;

{ TcsClassDescriber }

constructor TcsClassDescriber.Create;
begin
  // Intentionally left blank
end;

function TcsClassDescriber.IsApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean;
begin
  Result := True;
end;

class function TcsClassDescriber.IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean;
begin
  Result := False;
end;

procedure TcsClassDescriber.Assign(const ASourceDescriber: TcsClassDescriber);
begin
  // Nothing to do
end;

{ TcsDescriber }

procedure TcsDescriber.OverrideDescriptionForKnownClasses(AClass: TClass; ADescription: TcsStreamDescription);
var
  ClassDescriber: TcsClassDescriber;
begin
  if Assigned(FClassDescriberFactory) then
    ClassDescriber := FClassDescriberFactory.GetClassDescriberFor(AClass, nil, nil)
  else
    ClassDescriber := TcsClassDescriberFactory.Instance.GetClassDescriberFor(AClass, nil, nil);

  if Assigned(ClassDescriber) then
    ClassDescriber.AdaptDescription(ADescription, Self, AClass, nil, nil);
end;

constructor TcsDescriber.Create();
begin
  if FGlobalDefaultExportNameStyle<>tcsensNotSet then
    FDefaultExportNameStyle := FGlobalDefaultExportNameStyle;
  if FGlobalDefaultImportNameStyle<>tcsinsNotSet then
    FDefaultImportNameStyle := FGlobalDefaultImportNameStyle;
end;

destructor TcsDescriber.Destroy;
begin
  SetClassDescriberFactory(nil, False);
  inherited Destroy;
end;

procedure TcsDescriber.SetClassDescriberFactory(AClassDescriberFactory: TcsClassDescriberFactory; ABecomeOwner: Boolean);
begin
  if FIsClassDescriberFactoryOwner then
    FClassDescriberFactory.Free;
  FClassDescriberFactory := AClassDescriberFactory;
  FIsClassDescriberFactoryOwner := ABecomeOwner;
end;

procedure TcsDescriber.CloneGlobalClassDescriberFactory();
var
  Factory: TcsClassDescriberFactory;
begin
  Factory := TcsClassDescriberFactory.Create;
  Factory.Assign(TcsClassDescriberFactory.Instance);

  SetClassDescriberFactory(Factory, True);
end;

{ TcsStreamDescriptionList }

function TcsStreamDescriptionList.FindByPropertyName(const AName: string): TcsStreamDescription;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count -1 do
    if Items[i].PropertyName = AName then
      begin
      Result := Items[i];
      Break;
      end;
end;

procedure TcsStreamDescriptionList.Assign(ASource: TcsStreamDescriptionList);
var
  i: Integer;
begin
  for i := 0 to ASource.Count -1 do
    Add(ASource.Items[i].Clone);
end;

{ TcsStreamDescription }

destructor TcsStreamDescription.Destroy;
begin
  FProperties.Free;
  ListDescription := nil;
  DefaultSubObjectDescription := nil;
  inherited Destroy;
end;

function TcsStreamDescription.GetValueAsString(AnInstance: TObject): string;
begin
  if not Assigned(OnGetValueAsString) or not OnGetValueAsString(AnInstance, Self, Result) then
    Raise EcsStreamException.Create('No string value available');
end;

function TcsStreamDescription.GetValueAsInteger(AnInstance: TObject): Integer;
begin
  Raise EcsStreamException.Create('No integer value available');
end;

function TcsStreamDescription.GetValueAsInt64(AnInstance: TObject): Int64;
begin
  Result := GetValueAsInteger(AnInstance);
end;

function TcsStreamDescription.GetValueAsFloat(AnInstance: TObject): Extended;
begin
  Raise EcsStreamException.Create('No float value available');
end;

procedure TcsStreamDescription.SetValueAsString(AnInstance: TObject; Value: string);
begin
  if not Assigned(OnSetValueAsString) or not OnSetValueAsString(AnInstance, Self, Value) then
    Raise EcsStreamException.Create('It is not possible to assign a string-value to this item');
end;

procedure TcsStreamDescription.SetValueAsInteger(AnInstance: TObject; Value: Integer);
begin
  Raise EcsStreamException.Create('It is not possible to assign an integer-value to this item');
end;

procedure TcsStreamDescription.SetValueAsInt64(AnInstance: TObject; Value: Int64);
begin
  Raise EcsStreamException.Create('It is not possible to assign an Int64-value to this item');
end;

procedure TcsStreamDescription.SetValueAsFloat(AnInstance: TObject; Value: Extended);
begin
  Raise EcsStreamException.Create('It is not possible to assign a Float-value to this item');
end;

function TcsStreamDescription.GetItem(AnInstance: TObject; const Index: Integer): TcsStreamDescription;
begin
  Result := ListDescription;
end;

function TcsStreamDescription.GetItemCount(AnInstance: TObject): Int64;
begin
  if FStaticItemCount > -1 then
    Result := FStaticItemCount
  else if Assigned(OnGetItemCount) then
    Result := OnGetItemCount(AnInstance, Self)
  else
    Result := -1;
end;

function TcsStreamDescription.SetItemCount(AnInstance: TObject; AValue: Integer): Boolean;
begin
  if Assigned(OnSetItemCount) then
    Result := OnSetItemCount(AnInstance, Self, AValue)
  else
    Result := False;
end;

function TcsStreamDescription.GetEffectiveExportNameStyle: TcsExportNameStyle;
begin
  Result := ExportNameStyle;;
  if (Result=tcsensNotSet) and Assigned(Parent) then
    Result := Parent.GetEffectiveExportNameStyle;
end;

constructor TcsStreamDescription.Create(AParent: TcsStreamDescription);
begin
  FProperties := TcsStreamDescriptionList.Create(True);
  FParent := AParent;
  FStaticItemCount := -1;
  FIndex := -1;
  FIsWriteable := True;
  FIsReadable := True;
end;

function TcsStreamDescription.GetPropertyName: string;
begin
  Result := FPropertyName;
end;

procedure TcsStreamDescription.SetPropertyName(AValue: string);
begin
  FPropertyName := AValue;
end;

function TcsStreamDescription.GetValueAsObject(AnInstance: TObject): TObject;
begin
  if Assigned(OnGetValueAsObject) then
    Result := OnGetValueAsObject(AnInstance, Self)
  else
    Result := nil;
end;

procedure TcsStreamDescription.Assign(ASource: TcsStreamDescription);
begin
  InstanceType := ASource.InstanceType;
  ExportNameStyle := ASource.ExportNameStyle;
  ImportNameStyle := ASource.ImportNameStyle;
  if Assigned(ASource.ListDescription) then
    ListDescription := ASource.ListDescription.Clone;
  if Assigned(ASource.DefaultSubObjectDescription) then
    DefaultSubObjectDescription := ASource.DefaultSubObjectDescription.Clone;
  StaticItemCount := ASource.StaticItemCount;
  OnGetItemCount := ASource.OnGetItemCount;
  OnGetValueAsObject := ASource.OnGetValueAsObject;
  OnGetValueAsString := ASource.OnGetValueAsString;
  OnSetValueAsString := ASource.OnSetValueAsString;
  OnGetListObject := ASource.OnGetListObject;
  OnGetSubjectDescription := ASource.OnGetSubjectDescription;
  OnGetItemCount := ASource.OnGetItemCount;
  OnSetItemCount := ASource.OnSetItemCount;
  Index := ASource.Index;
  Properties.Assign(ASource.Properties);

  IsReadable := ASource.IsReadable;
  IsWriteable := ASource.IsWriteable;

  PropertyName := ASource.PropertyName;
end;

function TcsStreamDescription.Clone: TcsStreamDescription;
begin
  Result := TcsStreamDescriptionClass(Self.ClassType).Create(Parent);
  Result.Assign(Self);
end;

procedure TcsStreamDescription.SetListDescription(AValue: TcsStreamDescription);
begin
  if FListDescription = AValue then Exit;
  if Assigned(FListDescription) then
    FListDescription.Free;
  FListDescription := AValue;
end;

function TcsStreamDescription.GetEffectiveImportNameStyle: TcsImportNameStyle;
begin
  Result := ImportNameStyle;
  if (Result=tcsinsNotSet) and Assigned(Parent) then
    Result := Parent.GetEffectiveImportNameStyle;
end;

function TcsStreamDescription.GetListObject(AnInstance: TObject): TObject;
begin
  Result := nil;
  if Assigned(OnGetListObject) then
    Result := OnGetListObject(AnInstance, Self);
  if not Assigned(Result) then
    Result := AnInstance;
end;

function TcsStreamDescription.GetSubObjectDescription(AnInstance: TObject): TcsStreamDescription;
var
  OnGetDescription: TcsGetObjectDescription;
begin
  if UseOnGetSubjectDescription then
    begin
    OnGetDescription := GetEffectiveOnGetSubjectDescription;
    if Assigned(OnGetDescription) then
      Result := OnGetDescription(Self, AnInstance)
    else
      Result := DefaultSubObjectDescription;
    end
  else
    Result := DefaultSubObjectDescription;
end;

function TcsStreamDescription.GetEffectiveOnGetSubjectDescription: TcsGetObjectDescription;
begin
  Result := OnGetSubjectDescription;
  if not Assigned(Result) and Assigned(Parent) then
    Result := Parent.GetEffectiveOnGetSubjectDescription;
end;

function TcsStreamDescription.DateStringToDate(ADate: string): TDate;
begin
  if not TryISOStrToDate(ADate, Result) then
    Raise EcsStreamException.CreateFmt('Not a valid ISO-date: [%s]', [ADate]);
end;

function TcsStreamDescription.DateTimeStringToDate(ADateTime: string): TDateTime;
begin
  if not TryISOStrToDateTime(ADateTime, Result) then
    Raise EcsStreamException.CreateFmt('Not a valid ISO-datetime: [%s]', [ADateTime]);
end;

function TcsStreamDescription.FormatDateAsString(ADate: TDate): string;
begin
  Result := FormatDateTime('yyyy"-"mm"-"dd"', ADate);
end;

function TcsStreamDescription.FormatDateTimeAsString(ADateTime: TDateTime): string;
begin
  Result := FormatDateTime('yyyy"-"mm"-"dd"T"hh":"nn":"ss"."zzz', ADateTime);
end;

procedure TcsStreamDescription.SetValueAsBoolean(AnInstance: TObject; Value: Boolean);
begin
  Raise EcsStreamException.Create('It is not possible to assign an Int64-value to this item');
end;

procedure TcsStreamDescription.SetValueAsObject(AnInstance: TObject; Value: TObject);
begin
  Raise EcsStreamException.Create('It is not possible to assign an object-instance to this item');
end;

function TcsStreamDescription.GetSetValueAsObject(AnInstance: TObject): TObject;
begin
  Result := GetValueAsObject(AnInstance);
  if not Assigned(Result) and CanCreateClassInstances then
    begin
    Result := CreateNewInstance(AnInstance);
    if Assigned(Result) then
      SetValueAsObject(AnInstance, Result);
    end;
end;

function TcsStreamDescription.GetValueAsBoolean(AnInstance: TObject): Boolean;
begin
  Raise EcsStreamException.Create('No boolean value available');
end;

function TcsStreamDescription.CreateNewInstance(AParentInstance: TObject): TObject;
begin
  Result := nil;
  if not Assigned(OnCreateNewInstance) or not OnCreateNewInstance(Self, AParentInstance, Result) then
    Raise EcsStreamException.CreateFmt('It is not possible to create a new instance of a class for this item [%s]', [PropertyName]);
end;

procedure TcsStreamDescription.SetDefaultSubObjectDescription(AValue: TcsStreamDescription);
begin
  if FDefaultSubObjectDescription = AValue then Exit;
  if Assigned(FDefaultSubObjectDescription) then
    FDefaultSubObjectDescription.Free;
  FDefaultSubObjectDescription := AValue;
end;

end.

