unit csCollectionDescriber;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  csModel;

type
  TcsCollectionDescriberFlag = (
    tcsdfCollectionAsList);
  TcsCollectionDescriberFlags = set of TcsCollectionDescriberFlag;

  { TcsCollectionDescriber }

  TcsCollectionDescriber = class(TcsClassDescriber)
  private
    FFlags: TcsCollectionDescriberFlags;
  protected
    function GetTCollectionItemCount(AnInstance: TObject; ADescription: TcsStreamDescription): Integer;
    function GetTCollectionObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
    function GetTCollectionListObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
    function SetTCollectionItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
  public
    class function IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean; override;
    procedure AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject); override;
    procedure Assign(const ASourceDescriber: TcsClassDescriber); override;
    property Flags: TcsCollectionDescriberFlags read FFlags write FFlags;
  end;

implementation

{ TcsCollectionDescriber }

procedure TcsCollectionDescriber.AdaptDescription(var ADescription: TcsStreamDescription; ADescriber: TcsDescriber; AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject);
var
  Description: TcsStreamDescription;
begin
  if tcsdfCollectionAsList in Flags then
    begin
    ADescription.InstanceType := TcsitList;
    ADescription.OnGetListObject := @GetTCollectionListObject;
    ADescription.OnGetItemCount := @GetTCollectionItemCount;
    ADescription.OnSetItemCount := @SetTCollectionItemCount;

    Description := TcsStreamDescription.Create(ADescription);
    try
      Description.InstanceType := TcsitObject;
      Description.OnGetValueAsObject := @GetTCollectionObject;

      if (tcsdfDynamicClassDescriptions in ADescriber.Flags) then
        Description.UseOnGetSubjectDescription := True;
      ADescription.ListDescription := Description;
      Description := nil;
    finally
      Description.Free;
    end;
    end
  else
    begin
    Description := TcsStreamDescription.Create(ADescription);
    try
      Description.PropertyName := 'Items';
      Description.InstanceType := TcsitList;

      Description.ListDescription := TcsStreamDescription.Create(Description);
      Description.ListDescription.InstanceType := TcsitObject;
      Description.ListDescription.OnGetValueAsObject := @GetTCollectionObject;
      Description.OnGetItemCount := @GetTCollectionItemCount;
      Description.OnSetItemCount := @SetTCollectionItemCount;

      ADescription.Properties.Add(Description);
      Description := nil;
    finally
      Description.Free;
    end;
    end;
end;

class function TcsCollectionDescriber.IsGenerallyApplicableTo(AClass: TClass; AnInstance: TObject; AnInstanceParent: TObject): Boolean;
begin
  Result := AClass.InheritsFrom(TCollection);
end;

function TcsCollectionDescriber.GetTCollectionItemCount(AnInstance: TObject; ADescription: TcsStreamDescription): Integer;
begin
  if AnInstance is TCollection then
    Result := TCollection(AnInstance).Count
  else
    Result := -1;
end;

function TcsCollectionDescriber.GetTCollectionListObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
begin
  Result := ADescription.GetValueAsObject(AnInstance);
end;

function TcsCollectionDescriber.GetTCollectionObject(AnInstance: TObject; ADescription: TcsStreamDescription): TObject;
begin
  if AnInstance is TCollection then
    Result := TCollection(AnInstance).Items[ADescription.Index]
  else
    Result := nil;
end;

function TcsCollectionDescriber.SetTCollectionItemCount(AnInstance: TObject; ADescription: TcsStreamDescription; AValue: Integer): Boolean;
var
  i: Integer;
begin
  if AnInstance is TCollection then
    begin
    for i := 0 to AValue -1 do
      TCollection(AnInstance).Add;
    Result := True;
    end
  else
    Result := False;
end;

procedure TcsCollectionDescriber.Assign(const ASourceDescriber: TcsClassDescriber);
begin
  inherited Assign(ASourceDescriber);
  if ASourceDescriber is TcsCollectionDescriber then
    Flags := TcsCollectionDescriber(ASourceDescriber).Flags;
end;

end.

