{
   File generated automatically by Lazarus Package Manager
   Created with the Fppkgpackagemanager package installed

   fpmake.pp for cerialization 0.5.3

   This file was generated on 14-07-22
}

{$ifndef ALLPACKAGES}
{$mode objfpc}{$H+}
program fpmake;

uses fpmkunit;
{$endif ALLPACKAGES}

procedure add_cerialization(const ADirectory: string);

var
  P : TPackage;
  T : TTarget;
  D : TDependency;

begin
  with Installer do
    begin
    P:=AddPackage('cerialization');
    P.Version:='0.5.3-0';

    P.Directory:=ADirectory;

    P.Author:='Joost van der Sluis (CNOC)';
    P.License:='LGPL v2  with static linking exception';
    P.Description:='Library to (de)-serialize objects into/from JSON or other formats. (Currently only JSON is supported)';
    P.HomepageURL := 'https://gitlab.com/fpcprojects/cerialization';

    D := P.Dependencies.Add('log4fpc');
    D := P.Dependencies.Add('fcl-json');
    D := P.Dependencies.Add('rtl-generics');
    P.Options.Add('-MObjFPC');
    P.Options.Add('-Scghi');
    P.Options.Add('-O1');
    P.Options.Add('-g');
    P.Options.Add('-gl');
    P.Options.Add('-l');
    P.Options.Add('-vewnhibq');
    P.UnitPath.Add('.');
    P.ExamplePath.Add('tests');
    P.Targets.AddExampleProgram('cerializationtestrunner.pas');

    P.Sources.AddExample('tests/cerializationbasictests.pas');
    P.Sources.AddExample('tests/cerializationstringlisttest.pas');
    P.Sources.AddExample('tests/cerializationgenerictests.pas');
    P.Sources.AddExample('tests/cerializationtestrunner.lpi');
    P.Sources.AddExample('tests/cerializationtestrunner.lpi');
    P.Sources.AddSrc('cerialization.lpk');

    P.Sources.AddDoc('readme.md');
    P.Sources.AddDoc('COPYING.LGPL.txt');
    P.Sources.AddDoc('COPYING.modifiedLGPL.txt');
    T:=P.Targets.AddUnit('cerialization.pas');
    D := T.Dependencies.AddUnit('csModel');
    D := T.Dependencies.AddUnit('csStream');
    D := T.Dependencies.AddUnit('csJSONStream');
    D := T.Dependencies.AddUnit('csRttiModel');
    D := T.Dependencies.AddUnit('csStreamHelper');
    D := T.Dependencies.AddUnit('csJSONRttiStreamHelper');
    D := T.Dependencies.AddUnit('csStreamDescriptionStore');
    D := T.Dependencies.AddUnit('csCollectionDescriber');
    D := T.Dependencies.AddUnit('csStringListDescriber');
    D := T.Dependencies.AddUnit('csRegisterCommonClassDescribers');
    D := T.Dependencies.AddUnit('csGenericCollectionsDescriber');
    T := P.Targets.AddImplicitUnit('csmodel.pas');
    T := P.Targets.AddImplicitUnit('csstream.pas');
    T := P.Targets.AddImplicitUnit('csjsonstream.pas');
    T := P.Targets.AddImplicitUnit('csrttimodel.pas');
    T := P.Targets.AddImplicitUnit('csstreamhelper.pas');
    T := P.Targets.AddImplicitUnit('csjsonrttistreamhelper.pas');
    T := P.Targets.AddImplicitUnit('csstreamdescriptionstore.pas');
    T := P.Targets.AddImplicitUnit('cscollectiondescriber.pas');
    T := P.Targets.AddImplicitUnit('csstringlistdescriber.pas');
    T := P.Targets.AddImplicitUnit('csregistercommonclassdescribers.pas');
    T := P.Targets.AddImplicitUnit('csgenericcollectionsdescriber.pas');
    end;
end;

{$ifndef ALLPACKAGES}
begin
  add_cerialization('');
  Installer.Run;
end.
{$endif ALLPACKAGES}
