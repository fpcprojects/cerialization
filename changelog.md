### Changed
### Removed
### Added
### Fixed

## [0.5.3] - 2022-07-14
### Added
- Log basic deserialization actions with log4fpc
- Ability to create new instances of a class for a property

## [0.5.2] - 2021-08-06
### Fixed
- Fixed crash on deserializing explicit null-values.
- Fixed access violation when the tcsdfDynamicClassDescriptions flag is used.

## [0.5.1] - 2020-08-16
### Changed
- The csStreamDescription now owns it's ListDescription and DefaultSubObjectDescription. Making them part of the 'description-tree'. This means that it is no longer possible to assign them to a description directly from the description-store. Or else the description could get freed while still inside the store. You have to use a clone instead. This was done to fix several memory-leaks.