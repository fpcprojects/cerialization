# Cerialization

Library to store (serialize) the data within a component into streamable content like json, and vice-versa (deserialize). The package is not 'stable' yet, and the interface might change in the future.

## Getting started

These instructions will get you up-and-running for simple serialization-tasks. See the in-depth section for a more thorough understanding of how it works.

### Prerequisites

Cerialization only works with Free Pascal version 3.2 and up. To install it easily, the package-manager fppkg is used.

### Installing

Installation can be done from the central fppkg-repository:

    fppkg install cerialization

It is also possible to download the sources, start a command-line and make the source-directory the current-directory. Then type:

    fppkg install

Now all units from the Cerialization-package can be used in your Free Pascal applications.

### Basic usage

An object is serialized into a json-string by `TJSONRttiStreamClass.ObjectToJSONString(AnObject)`, the other way around is done by `TJSONRttiStreamClass.JSONStringToObject(JSONString, AnObject)`. Like is done in this full example:


```
program StreamExample1;

uses
  Classes,
  csJSONRttiStreamHelper;

var
  Comp: TComponent;
  StreamClass: TJSONRttiStreamClass;
  S: string;
begin
  StreamClass := TJSONRttiStreamClass.Create;
  try
    Comp := TComponent.Create(nil);
    try
      // Stream the JSON-string to the Component
      StreamClass.JSONStringToObject('{"Name":"OrigName"}', Comp);

      // This prints "OrigName"
      Writeln('The original name: ', Comp.Name);
      Comp.Name := 'NewName';

      // Stream the Compontent to a JSON-string
      S := StreamClass.ObjectToJSONString(Comp);

      // This prints:
      // { "Name" : "NewName", "Tag" : 0 }
      writeln(S);
    finally
      Comp.Free;
    end;
  finally
    StreamClass.Free;
  end;
end.
```

#### Handle upper- and/or lower-case JSON-keys

It might be that a json-key need an uppercase character while a property in your class is lowercase or vice-versa. To handle these differences there are the `ExportNameStyle` and `ImportNameStyle` properties for serialization and deserialization respectively. They are defined in the csModel unit. It is also possible to set defaults values. You have to do this before any object is (de)-serialized:

```
StreamClass.Describer.DefaultImportNameStyle := tcsinsCaseInsensitive;
StreamClass.Describer.DefaultExportNameStyle := tcsensLowerCaseFirstChar;
```

To read more about the Describer, check the In-Depth section below.

#### Trick to serialize keys which are reserved words in Pascal

It is possible to stream properties with keywords that correspond to reserved words in pascal, like 'type', 'begin', 'end' or 'program'. It is possible to escape these keywords using a `&` sign. This is not a feature of Cerialization, but a feature of the Free Pascal compiler. A class-definition using this trick may look like this:

```
type
  TMyClass = class
  private
    FType: string;
  published
    property &Type: string read FType write FType;
  end;
```

#### Solution without the StreamClass (less code)

There is also another approach which need less code in your applications. This might not be suitable for applications that to a lot of (de-)serialization, as it ihas lower configuration-options and  is slower. It works with class-procedures of the `TJSONRttiStreamHelper` class. See this example:

```
program StreamExample2;

uses
  Classes,
  csModel,
  csJSONRttiStreamHelper;

var
  Comp: TComponent;
  S: string;

begin
  Comp := TComponent.Create(nil);
  try
    // It is also possible to change the Lower/Upper-case behaviour with the class-procedure approach:
    TcsDescriber.GlobalDefaultExportNameStyle := tcsensLowerCase;
    TcsDescriber.GloblaDefaultImportNameStyle := tcsinsCaseInsensitive;

    // Stream the JSON-string to the Component
    TJSONRttiStreamHelper.JSONStringToObject('{"name":"OrigName"}', Comp);

    // This prints "OrigName"
    Writeln('The original name: ', Comp.Name);
    Comp.Name := 'NewName';

    // Stream the Compontent to a JSON-string
    S := TJSONRttiStreamHelper.ObjectToJSONString(Comp);

    // This prints:
    // { "name" : "NewName", "tag" : 0 }
    writeln(S);
  finally
    Comp.Free;
  end;
end.
```

## In Depth explanation on how things work

This section give a better understanding on the concepts Cerialization is based on, and how things work internally.

### Basic components

To understand how Cerialization works, you have to know what the basic components are:

#### Descriptions

Classes are serialized based on a description. A description holds both the definition of the properties that should be streamed, as the logic to retrieve and set the actual data from an instance of a class. By adapting a description it is possible to change the way a class is serialized. The base class of any description is `TcsStreamDescription`.

#### Describers

Because it is cumbersome to write a description for each and every class you want to (de-)serialize, a describer can do this work for you. Most common is probably a describer that creates a description based on RTTI-information. Such a describer can create a description for any class, just based on the run-time-type-information (RTTI). Describers are derrived from the `TcsDescriber` class.

#### Description-store

Once a describer has made a description, it would be nice to store the description for later use. This is what the description-store is for. It can hold descriptions for multiple classes. For each class different descriptions are stored coupled to custom tags. For example it would be possible to create two descriptions for the TComponent class, one with an empty tag, and one with the tag 'OmitTagProperty'.
To make it easier to define multiple descriptions for the same class, descriptions can be cloned and thereafter adapted.

#### Modelstreams

The modelstreams are the worker-classes that do the actual work. There could be different modelstreams for other formats to stream to/from. At this moment there is only one modelstreamer, `TcsJSONStream`, which streams to json, using Free Pascal's fpjson library.

#### Streamclasses

Streamclasses bind everything together. The `TJSONRttiStreamClass` for example, uses a RTTI-based describer to create descriptions for classes when they are (de-)serialized for the first time. These descriptions are stored inside a description-store. A json-based modelstream is used to de-serialize the objects based on the stored descriptions.

## Advanced topics

### Automatic creation of object-instances.

Consider a class like this:

```
type
  TSubObjectObject = class
  private
    FObj1: TMyClass;
  published
    property Obj1: TMyClass read FObj1 write FObj1;
  end;

```

It is possible to deserialize JSON in such a way that `JSONStringToObject` creates a `TMyClass` instance for `Obj1`. To do this, the description of the Obj1-property needs `CanCreateClassInstances` set to true. When streaming using RTTI, ths base constructor (`create`) of the class is used to initiate an instance. This behavior can be overridden with the `TcsStreamDescription.OnCreateNewInstance` event.
Note that the instance is automatically created, but not freed.

Example: (Type definitions are omitted)

```
program StreamExample3;

uses
  csModel,
  csJSONRttiStreamHelper;

var
  Obj: TSubObjectObject;
  StreamClass: TJSONRttiStreamClass;
  S: string;
  Descr: TcsStreamDescription;
begin
  StreamClass := TJSONRttiStreamClass.Create;
  try
    Obj := TSubObjectObject.Create;
    try
      // Adapt the default description for the TTestSubObjectObject class
      Descr := StreamClass.DescriptionStore.GetDescription(TSubObjectObject);
      // Enable automatic creation of TTestSubObjectObject-instances
      Descr.Properties.FindByPropertyName('Obj1').CanCreateClassInstances:=True;
      // Optional: set an event to handle the object-creation
      // Descr.Properties.FindByPropertyName('Obj1').OnCreateNewInstance:=@CreateNewInstance;

      // Deserialize
      StreamClass.JSONStringToObject('{"Obj1":{"Type":"TypeValue"}}', Obj);

      // This prints "TypeValue"
      Writeln('The type-value of the newly created sub-object: ', Obj.Obj1.&Type);
      writeln(S);

      // Free the automatically created object. But better do this in a
      // destructor. (TSubObjectObject.Destroy)
      Obj.Obj1.Free;
    finally
      Obj.Free;
    end;
  finally
    StreamClass.Free;
  end;
end.
```
<!--
### Filtering
 -->
## Contributing

Questions, ideas and patches are welcome. You can send them directly to <joost@cnoc.nl>, or discuss them at the fpc-pascal mailinglist.

## Versioning

This package is very new and receives a lot of updates. For the versions available, see the tags on this repository.

## Authors

* **Joost van der Sluis** - *initial work* - <joost@cnoc.nl>

License
=======
This library is distributed under the Library GNU General Public License version 2 (see the [COPYING.LGPL.txt](COPYING.LGPL.txt) file) with the following modification:

- object files and libraries linked into an application may be distributed without source code. As further eplained in the file [COPYING.modifiedLGPL.txt](COPYING.modifiedLGPL.txt).