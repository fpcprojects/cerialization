unit csStreamHelper;

{$mode objfpc}{$H+}

interface

uses
  csModel,
  csStream,
  csStreamDescriptionStore;

type

  { TcsStreamHelper }

  TcsStreamHelper = class
  protected
    class function ObtainDescriber(): TcsDescriber; virtual; abstract;
    class function ObtainModelStream(): TcsModelStream; virtual; abstract;
  public
    class function ObjectToModel(AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []): TObject;
    class procedure ModelToObject(AModel: TObject; AnInstance: TObject; ADescription: TcsStreamDescription = nil; ADescriberFlags: TcsDescriberFlagSet = []);
  end;
  TcsStreamHelperClass = class of TcsStreamHelper;

  { TcsStreamClass }

  TcsStreamClass = class
  private
    FDescriptionStore: TcsStreamDescriptionStore;
    FDescriber: TcsDescriber;
  protected
    function GetStreamHelper: TcsStreamHelperClass; virtual; abstract;
    function InitializeDescriber: TcsDescriber; virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    property DescriptionStore: TcsStreamDescriptionStore read FDescriptionStore;

    function ObjectToModel(AnInstance: TObject; ADescriptionTag: string = ''): TObject;
    procedure ModelToObject(AModel: TObject; AnInstance: TObject; ADescriptionTag: string = '');
    property Describer: TcsDescriber read FDescriber;
  end;

implementation

{ TcsStreamClass }

constructor TcsStreamClass.Create;
begin
  FDescriptionStore := TcsStreamDescriptionStore.Create;
  FDescriber := InitializeDescriber;
  FDescriptionStore.Describer := FDescriber;
end;

destructor TcsStreamClass.Destroy;
begin
  FDescriber.Free;
  FDescriptionStore.Free;
  inherited Destroy;
end;

procedure TcsStreamClass.ModelToObject(AModel: TObject; AnInstance: TObject; ADescriptionTag: string);
begin
  GetStreamHelper.ModelToObject(AModel, AnInstance, FDescriptionStore.GetDescription(AnInstance.ClassType, ADescriptionTag));
end;

function TcsStreamClass.ObjectToModel(AnInstance: TObject; ADescriptionTag: string): TObject;
begin
  Result := GetStreamHelper.ObjectToModel(AnInstance, FDescriptionStore.GetDescription(AnInstance.ClassType, ADescriptionTag));
end;

function TcsStreamClass.InitializeDescriber: TcsDescriber;
begin
  Result := GetStreamHelper.ObtainDescriber();
end;

{ TcsStreamHelper }

class function TcsStreamHelper.ObjectToModel(AnInstance: TObject; ADescription: TcsStreamDescription; ADescriberFlags: TcsDescriberFlagSet = []): TObject;
var
  Describer: TcsDescriber;
  DescriptionStore: TcsStreamDescriptionStore;
  ModelStream: TcsModelStream;
  Description: TcsStreamDescription;
  Serializer: TcsStream;
begin
  Describer := ObtainDescriber();
  try
    if tcsdfDynamicClassDescriptions in ADescriberFlags then
      begin
      DescriptionStore := TcsStreamDescriptionStore.Create;
      DescriptionStore.Describer := Describer;
      end
    else
      DescriptionStore := nil;
    try
      Describer.Flags := ADescriberFlags;
      if Assigned(ADescription) then
        Description:=ADescription
      else
        begin
        // When the Describer's tcsdfDynamicClassDescriptions flag is set, a
        // descriptionstore is used, so it can provide the dynamic class
        // descriptions.
        if Assigned(DescriptionStore) then
          Description := DescriptionStore.GetDescription(AnInstance.ClassType)
        else
          Description := Describer.ObtainDescriptionForClass(AnInstance.ClassType, nil);
        end;
      try
        ModelStream := ObtainModelStream();
        try
          Serializer := TcsStream.Create(Description, ModelStream);
          try
            Result := Serializer.ObjectToModel(AnInstance);
          finally
            Serializer.Free;
          end;
        finally
          ModelStream.Free;
        end;
      finally
        if not Assigned(ADescription) and not Assigned(DescriptionStore) then
          Description.Free;
      end;
    finally
      DescriptionStore.Free;
    end;
  finally
    Describer.Free;
  end;
end;

class procedure TcsStreamHelper.ModelToObject(AModel: TObject; AnInstance: TObject; ADescription: TcsStreamDescription; ADescriberFlags: TcsDescriberFlagSet = []);
var
  Describer: TcsDescriber;
  ModelStream: TcsModelStream;
  Description: TcsStreamDescription;
  DeSerializer: TcsStream;
begin
  Describer := ObtainDescriber();
  try
    Describer.Flags := ADescriberFlags;
    if Assigned(ADescription) then
      Description:=ADescription
    else
      Description := Describer.ObtainDescriptionForClass(AnInstance.ClassType, nil);
    try
      ModelStream := ObtainModelStream();
      try
        DeSerializer := TcsStream.Create(Description, ModelStream);
        try
          DeSerializer.ModelToObject(AModel, AnInstance);
        finally
          DeSerializer.Free;
        end;
      finally
        ModelStream.Free;
      end;
    finally
      if not Assigned(ADescription) then
        Description.Free;
    end;
  finally
    Describer.Free;
  end;
end;

end.

